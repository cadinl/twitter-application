
/**
*SQL to create test TwitterDB
 * Author:  cadin
 * Created: Nov 10, 2019
 */

DROP DATABASE IF EXISTS TESTTWITTER;
CREATE DATABASE TESTTWITTER;


DROP USER IF EXISTS testUser@localhost;
CREATE USER testUser@'localhost' IDENTIFIED WITH mysql_native_password BY 'test' REQUIRE NONE;
GRANT ALL ON TESTTWITTER.* TO testUser@'localhost';



FLUSH PRIVILEGES;
