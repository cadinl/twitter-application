
/**
* SQL SCRIPT TO CREATE DATABASE 
 * Author:  cadin
 * Created: Nov 2, 2019
 */
DROP DATABASE IF EXISTS TWITTER;
CREATE DATABASE TWITTER;

DROP USER IF EXISTS twitterUser@localhost;
CREATE USER twitterUser@'localhost' IDENTIFIED WITH mysql_native_password BY 'twitter' REQUIRE NONE;
GRANT ALL ON TWITTER.* TO twitterUser@'localhost';



FLUSH PRIVILEGES;

