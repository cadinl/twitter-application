/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadinlondono.twitterApplication.tests;

import com.cadinlondono.twitterApplication.business.tweetBeans.DatabaseTweet;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import com.cadinlondono.twitterApplication.persistance.TweetsDAO;
import com.cadinlondono.twitterApplication.persistance.TweetsDAOImplementation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cadin
 */
public class TweetsDAOTests {
    
    private final String url = "jdbc:mysql://localhost:3306/TESTTWITTER?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true";
    private final String user = "testUser";
    private final String password = "test";
    
    //private final static Logger LOG = LoggerFactory.getLogger(TweetsDAOTests.class);

    /**
     * this method uses the findAll method in the TweetsDAO class
     * to get all default tweets(5 tweets)
     * and sees that the list of tweets returned 
     * is of size 5
     * @throws SQLException
     * @throws IOException 
     */
    @Test(timeout = 1000)
    public void testFindAll() throws SQLException, IOException {
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        List<Tweet> tweets = tweetsDAO.findAll();

        assertEquals("# of tweets", 5, tweets.size());
    }
    
    /**
     * this method creates a tweet and stores it in the db
     * then checks if it was saved in the db
     * @throws SQLException
     * @throws IOException 
     */
    @Test(timeout = 1000)
    public void testCreateTweet() throws SQLException, IOException{
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        Tweet tweet = new DatabaseTweet("test name","tweet test", "imageURL", "testHandle", 6);
        tweetsDAO.createTweet(tweet);
        assertTrue("the tweet wasn't saved on the tweets table",tweetsDAO.isTweetAlreadySaved(tweet.getTweetID()));

    }
    
    /**
     * this method deletes one of the default test tweets
     * and checks if the tweet is still saved in the db(this should return false)
     * @throws SQLException
     * @throws IOException 
     */
    //timeout higher since randomly it takes more time
    @Test(timeout = 3000)
    public void testDeleteTweet() throws SQLException, IOException{
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        tweetsDAO.deleteTweet(1);
        assertFalse("the tweet wasn't delete on the tweets table",tweetsDAO.isTweetAlreadySaved(1));
        
    }
    /**
     * This test checks that the method isTweetAlreadySaved
     * return true that the tweet with id 1 is saved.
     * the tweet with id 1 is always there because the default 
     * values are always initialized before each test
     * @throws SQLException
     * @throws IOException 
     */
    @Test(timeout = 1000)
    public void testIsTweetAlreadySaved() throws SQLException, IOException{
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        assertTrue("the method tweetIsAlreadySave didn't properly returned that the 1st tweet is saved", tweetsDAO.isTweetAlreadySaved(1));
    }
    /**
     * This tweet checks that the method tweetIsAlreadySaved
     * doesn't return true that the tweet with id 100 is saved
     * (it is not part of default values)
     * @throws SQLException
     * @throws IOException 
     */
    @Test(timeout = 1000)
    public void testIsTweetNotAlreadySaved() throws SQLException, IOException{
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        assertFalse("the method tweetisalready saved didn't properly returned that the tweet isn't saved", tweetsDAO.isTweetAlreadySaved(100));
    }
    
    /**
     * the twitter handle character limit is 15. we want the DAO
     * to throw an SQL exception if the name exceeds this limit
     * @throws SQLException 
     */
    @Test(timeout = 1000, expected = SQLException.class)
    public void testCreateFailureHandleLength() throws SQLException {
        Tweet tweet = new DatabaseTweet("test name", "test text", "imageURL", "test handle that is more than 15 characters", 10);
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        tweetsDAO.createTweet(tweet);
        fail("tweetsDAO didn't throw an exception even if handle limit exceed");
    }
    /**
     * the twitter name character limit is 50. we want the DAO
     * to throw an SQL exception if the name exceeds this limit
     * @throws SQLException 
     */
    public void testCreateFailureNameLength() throws SQLException {
        Tweet tweet = new DatabaseTweet("name exceeds 50 character limit ///////////////////", "test text", "imageURL", "test handle", 10);
        TweetsDAO tweetsDAO = new TweetsDAOImplementation(this.url, this.user, this.password);
        tweetsDAO.createTweet(tweet);
        fail("tweetsDAO didn't throw an exception even if name limit exceed");
    }


    
    /**
     * This code is credited to Ken Fogel
     * original script: https://gitlab.com/omniprof/fish_form_validated_demo/blob/master/src/test/java/com/kenfogel/fishform/tests/FishV2DAOTest.java
	 * This routine recreates the database for every test. This makes sure that
	 * a destructive test will not interfere with any other test.
	 * 
	 * This routine is courtesy of Bartosz Majsak, an Arquillian developer at
	 * JBoss who helped me out last winter with an issue with Arquillian. Look
	 * up Arquillian to learn what it is.
	 */
	@Before
	public void seedDatabase() {
            //recreating the table
		final String seedDataScript = loadAsString("CreateTweetTable.sql");
		try (Connection connection = DriverManager.getConnection(url, user, password);) {
			for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                                connection.prepareStatement(statement).execute();
			}
		} catch (SQLException e) {
			throw new RuntimeException("Failed seeding database", e);
		}
               
            //inserting the default test values
            final String seedDataScriptInsert = loadAsString("TestDefaultTweetRows.sql");
		try (Connection connection = DriverManager.getConnection(url, user, password);) {
			for (String statement : splitStatements(new StringReader(seedDataScriptInsert), ";")) {
				connection.prepareStatement(statement).execute();
			}
		} catch (SQLException e) {
			throw new RuntimeException("Failed seeding database", e);
		}
	}

	/**
         * Credit: Ken Fogel
	 * The following methods support the seedDatabse method
	 */
	private String loadAsString(final String path) {
		try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
				Scanner scanner = new Scanner(inputStream)) {
			return scanner.useDelimiter("\\A").next();
		} catch (IOException e) {
			throw new RuntimeException("Unable to close input stream.", e);
		}
	}

	private List<String> splitStatements(Reader reader, String statementDelimiter) {
		final BufferedReader bufferedReader = new BufferedReader(reader);
		final StringBuilder sqlStatement = new StringBuilder();
		final List<String> statements = new LinkedList<>();
		try {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty() || isComment(line)) {
					continue;
				}
				sqlStatement.append(line);
				if (line.endsWith(statementDelimiter)) {
					statements.add(sqlStatement.toString());
					sqlStatement.setLength(0);
				}
			}
			return statements;
		} catch (IOException e) {
			throw new RuntimeException("Failed parsing sql", e);
		}
	}

	private boolean isComment(final String line) {
		return line.startsWith("--") || line.startsWith("//") || line.startsWith("/*");
	}


    
}
