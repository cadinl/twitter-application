package com.cadinlondono.twitterApplication.persistance;

import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import java.sql.SQLException;
import java.util.List;

/**
 *TweetsDAO interface.we want to have an interface in case we
 * we want to change implementation, we don't need to change the rest of 
 * the code
 * @author cadin
 */
public interface TweetsDAO {
    
    public void createTweet(Tweet tweet) throws SQLException;
    public void deleteTweet(long tweetID) throws SQLException;
    public List<Tweet> findAll() throws SQLException;
    public boolean isTweetAlreadySaved(long tweetID) throws SQLException;
}
