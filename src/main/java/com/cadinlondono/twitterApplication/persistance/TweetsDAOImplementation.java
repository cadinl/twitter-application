
package com.cadinlondono.twitterApplication.persistance;

import com.cadinlondono.twitterApplication.business.tweetBeans.DatabaseTweet;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import java.io.IOException;
import java.io.InputStream;
import static java.nio.file.Files.newInputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *The implementation of the TweetsDAO interface.
 * this implementation works for a mysql database
 * @author cadin
 */
public class TweetsDAOImplementation implements TweetsDAO{
    
    private final static Logger LOG = LoggerFactory.getLogger(TweetsDAOImplementation.class);

    
    private final String URL;//DB authentication
    private final String user;//DB authentication
    private final String password;//DB authentication
    private final String propertiesPath = "src/main/resources";// path to DatabaseCredentials
    private final String propFileName = "DatabaseCredentials";//were we get the default url,user and password

    /**
     * default constructor with no parameter, will load database credentials
     * from properties file in resources (DatabaseCredentials.properties)
     * @throws IOException 
     */
    public TweetsDAOImplementation() throws IOException{
        
        Properties prop = new Properties();
        
        Path txtFile = get(propertiesPath, propFileName + ".properties");
        
        try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
        
        this.URL = prop.getProperty("URL");
        this.user = prop.getProperty("User");
        this.password = prop.getProperty("Password");
            

    }
    /**
     * this constructor is used if we want to use a different database or user
     * than the one in the properties file
     * @param URL
     * @param user
     * @param password 
     */
    public TweetsDAOImplementation(String URL, String user, String password){
        this.URL = URL;
        this.user = user;
        this.password = password;
    }

    /**
     * Stores a tweet in the database
     * @param tweet
     * @throws SQLException 
     */
    @Override
    public void createTweet(Tweet tweet) throws SQLException {
        String createQuery = "INSERT INTO TWEETS (NAME, HANDLE, IMAGEURL, TWEETID, TEXT) VALUES (?,?,?,?,?)";
        
         try (Connection connection = DriverManager.getConnection(URL, user, password);
                //prepared statement
                PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForCreatingTweet(ps, tweet);
            ps.executeUpdate();
         }


    }

    /**
     * returns a list of tweets of all the tweets saved in the database
     * @return List
     * @throws SQLException 
     */
    @Override
    public List<Tweet> findAll() throws SQLException{
        
        List<Tweet> rows = new ArrayList<>();
        
        String selectQuery = "SELECT * FROM TWEETS";
        
        try (Connection connection = DriverManager.getConnection(URL, user, password);
                //prepared statement
                PreparedStatement ps = connection.prepareStatement(selectQuery);
                ResultSet resultSet = ps.executeQuery()) {
            while (resultSet.next()) {
                rows.add(createTweet(resultSet));
            }
        }
        LOG.info("# of records found : " + rows.size());
        return rows;

    }
    
    /**
     * returns true if the tweetID provided is of one of the
     * tweets is stored in the database, else returns false
     * @param tweetID
     * @return boolean
     * @throws SQLException 
     */
    @Override
    public boolean isTweetAlreadySaved(long tweetID) throws SQLException {
        
        String findQuery = "SELECT COUNT(*) AS NumberOfTweets FROM TWEETS WHERE TWEETID = ? ";
        
        try (Connection connection = DriverManager.getConnection(URL, user, password);
                //prepared statement
                PreparedStatement ps = connection.prepareStatement(findQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForTweetID(ps, tweetID);
            ResultSet resultSet = ps.executeQuery();
            if(resultSet.next())
            {
                int numberOfTweets = resultSet.getInt("NumberOfTweets");
                if(numberOfTweets == 1)
                {
                    return true;
                }
                else if(numberOfTweets == 0)
                {
                    return false;
                }
                else
                {
                    throw new SQLException("More than one tweet was found with same id");
                }
            }
         }
        throw new SQLException("couldn't retrieve information from database");
    }
    
    /**
     * delete the tweet that has the tweetID provided
     * @param tweetID
     * @throws SQLException 
     */
    @Override
    public void deleteTweet(long tweetID) throws SQLException {
       String createQuery = "DELETE FROM TWEETS WHERE TWEETID = ?";
        
         try (Connection connection = DriverManager.getConnection(URL, user, password);
                //prepared statement
                PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS);) {
            fillPreparedStatementForTweetID(ps, tweetID);
            ps.executeUpdate();
         }

    }
    
    //fills up the prepared statement of the methods that only need the tweetID to be set
    private void fillPreparedStatementForTweetID(PreparedStatement ps, long tweetID) throws SQLException{
        ps.setLong(1, tweetID);
    }
    //fils up the prepared statement for the create tweet method
    private void fillPreparedStatementForCreatingTweet(PreparedStatement ps, Tweet tweet) throws SQLException{
        ps.setString(1, tweet.getName());
        ps.setString(2, tweet.getHandle());
        ps.setString(3, tweet.getImageURL());
        ps.setLong(4, tweet.getTweetID());
        ps.setString(5, tweet.getText());
    }
    
    //tranforms a result set from the database into a tweet object
    //used for retrieving tweets
    private Tweet createTweet(ResultSet resultSet) throws SQLException{
        if(resultSet == null)
        {
            LOG.debug("result set null");
        }
        else
        {
            LOG.debug("result set not null");
        }
        DatabaseTweet tweet = new DatabaseTweet();
        tweet.setName(resultSet.getString("NAME"));
        tweet.setHandle(resultSet.getString("HANDLE"));
        tweet.setImageURL(resultSet.getString("IMAGEURL"));
        tweet.setTweetID(resultSet.getLong("TWEETID"));
        tweet.setText(resultSet.getString("TEXT"));
        return (Tweet)tweet;
    }

    

}
