package com.cadinlondono.twitterApplication.constants;

/**
 * Repetive constants use through out the application
 * @author cadin
 */
public class TwitterConstants {
    
    public static final int MAXDISPLAYOFSTATUSES = 20;
    public static final String PROFILE_FXML_PATH = "/fxml/page/ProfilePage.fxml";
    public static final String TWITTER_BUNDLE = "TwitterBundle"; 
    public static final String DM_UTILITY_FXML_PATH = "/fxml/utils/directMessageUtility.fxml";
    
    private TwitterConstants(){}
}
