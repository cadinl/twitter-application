
package com.cadinlondono.twitterApplication.business;

import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.Status;
import twitter4j.StatusUpdate;

/**
 * This engine is part of the engines that use
 * twitter4j to provide the essential information
 * to this twitter application.
 * The tweet engine more specifically adds the functionality we
 * need to a tweet in our application
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class TweetEngine {
    
    private Twitter twitterInstance;
    private Status status;
    private Tweet tweet;
    
    /**
     * this constructor takes as input the id
     * of the status we want to add function to 
     * in the context of our application
     * @param statusId
     * @throws TwitterException 
     */
    public TweetEngine(Tweet tweet) throws TwitterException
    {
        this.tweet = tweet;
        this.twitterInstance = getTwitterInstance();
        this.status = getStatusById(tweet.getTweetID());
               
    }
    
    private Twitter getTwitterInstance() {
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }
    
    private Status getStatusById(long id) throws TwitterException{
        if(this.twitterInstance == null)
        {
            this.twitterInstance = getTwitterInstance();
        }
        
        return this.twitterInstance.showStatus(id);
    }
    /**
     * this method will retweet the tweet that the engine has been given
     * in its constructor
     * @throws TwitterException 
     */
    public void retweetTweet() throws TwitterException{
        this.twitterInstance.retweetStatus(status.getId());
    }
    /**
     * this method will retweet the tweet with a message
     * that the engine has been given
     * in its constructor
     * @throws TwitterException 
     */
    public void retweetTweetWithMessage(String message) throws TwitterException{
        String tweetUrl = "https://twitter.com/" + this.status.getUser().getScreenName()
                        + "/status/" + status.getId();
        
        String messageWithUrl = message + " " + tweetUrl;
        //test number of characters
        this.twitterInstance.updateStatus(messageWithUrl);
    }
    /**
     * this will let you add a reply to the tweet that you have given
     * in constructor
     * @param tweetMessage
     * @throws TwitterException 
     */
    public void replyToTweet(String tweetMessage) throws TwitterException{
        StatusUpdate statusUpdate = new StatusUpdate(tweetMessage);
        statusUpdate.inReplyToStatusId(this.status.getId());
        this.twitterInstance.updateStatus(statusUpdate);
    }

    
    /**
     * returns the number of times the tweet has been retweeted
     * @return int
     * @throws TwitterException 
     */
    public int getRetweetCount() throws TwitterException{
        return this.status.getRetweetCount();       
    }
    /**
     * returns the number of times the tweet has been liked
     * @return int
     * @throws TwitterException 
     */
    public int getFavoriteCount() throws TwitterException{
        return this.status.getFavoriteCount();
    }
    


    /**
     * returns if is favorited
     * @return boolean
     * @throws TwitterException 
     */    
    public boolean isFavorite() throws TwitterException{
        return this.status.isFavorited();
    }
    
    /**
     * favorites the tweet(like)
     * @throws TwitterException 
     */
    public void favoriteTweet() throws TwitterException{
        this.twitterInstance.createFavorite(this.status.getId());
    }
    
    /**
     * unfavorites the tweet(unlike)
     * @throws TwitterException 
     */
    public void unfavoriteTweet() throws TwitterException{
        this.twitterInstance.destroyFavorite(this.status.getId());
    }
    /**
     * returns the user object of the user that tweeted this tweet
     * @return User userObject
     * @throws TwitterException 
     */
    public User getUser() throws TwitterException{
        //THIS ONE NEEDS TO BE USER BECAUSE WE NEED THE OBJECT TO CREATE THE 
        //PROFILE PAGE USING THE USER ENGINE
        return this.status.getUser();
    }
    /**
     * returns the message of the tweet
     * @return String tweetMessage
     * @throws TwitterException 
     */
    public String getTweetMessage() throws TwitterException{
        return this.tweet.getText();
    }
    
}
