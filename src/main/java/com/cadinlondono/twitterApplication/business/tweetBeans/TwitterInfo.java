
package com.cadinlondono.twitterApplication.business.tweetBeans;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.Status;

/**
 *This is the implementation of the tweet interface
 * that we use when we get tweets directly from the
 * twitter4j API
 * @author Cadin Londono
 */
public class TwitterInfo implements Tweet, Serializable{
     private final static Logger LOG = LoggerFactory.getLogger(TwitterInfo.class);
    
    private final String name;
    private final String text;
    private final String imageURL;
    private final String handle;
    private final long tweetID;

    public TwitterInfo(Status status) {
        this.name = status.getUser().getName();
        this.text = status.getText();
        this.imageURL = status.getUser().getProfileImageURL();
        this.handle = status.getUser().getScreenName();
        this.tweetID = status.getId();
    }
    
    /**
     * returns the name of the user that tweeted the tweet
     * @return String
     */
    @Override
    public String getName() {
        return this.name;
    }
    /**
     * returns the text of the tweets
     * @return String
     */
    @Override
    public String getText(){
        return this.text;
    }
    /**
     * returns the imageURL of the profile picture of the user
     * @return String
     */
    @Override
    public String getImageURL(){
        return this.imageURL;
    }
    /**
     * returns the handle of the user that tweeted the tweet
     * @return String
     */
    @Override
    public String getHandle() {
      return this.handle;
    }
    /**
     * returns the tweetID of the tweet
     * @return long
     */
    @Override
    public long getTweetID() {
        return this.tweetID;
    }
}


