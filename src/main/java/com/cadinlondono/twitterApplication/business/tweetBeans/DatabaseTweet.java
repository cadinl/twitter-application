
package com.cadinlondono.twitterApplication.business.tweetBeans;

import java.io.Serializable;
import java.util.Objects;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *This object is use when we want to retrieve a tweet
 * from the database and display it as a saved tweet
 * @author cadin
 */
public class DatabaseTweet implements Tweet, Serializable{
    private StringProperty name;
    private StringProperty text;
    private StringProperty imageURL;
    private StringProperty handle;
    private LongProperty tweetID;
    
    /**
     * this constructor is used if we want to set all the values
     * directly when creating the object
     * @param name
     * @param text
     * @param imageURL
     * @param handle
     * @param tweetID 
     */
    public DatabaseTweet(String name, String text, String imageURL, String handle, long tweetID){
        this.name = new SimpleStringProperty(name);
        this.text = new SimpleStringProperty(text);
        this.imageURL = new SimpleStringProperty(imageURL);
        this.handle = new SimpleStringProperty(handle);
        this.tweetID = new SimpleLongProperty(tweetID);
    }
    /**
     * this constructor is used if we want to set the values of the tweet
     * after is creation. to escape nullPointerExceptions all strings are set to
     * "" and the id is set to -1
     */
    public DatabaseTweet(){
        this(" ", " ", "", " ", -1);
    }
    /**
     * returns the name of the user that tweeted the tweet
     * @return String
     */
    @Override
    public String getName(){
        return this.name.get();
    }
    /**
     * returns the text of the tweet
     * @return String
     */
    @Override
    public String getText(){
        return this.text.get();
    }
    /**
     * returns the imageURL of the profile pic of the user
     * @return String
     */
    @Override
    public String getImageURL(){
        return this.imageURL.get();
    }
    /**
     * returns the handle of the user that tweeted the tweet
     * @return String
     */
    @Override
    public String getHandle(){
        return this.handle.get();
    }
    /**
     * returns the tweetId of the tweet
     * @return long
     */
    @Override
    public long getTweetID(){
        return this.tweetID.get();
    }
    /**
     * sets the name of the user that tweeted the tweet
     * @param name
     */
    public void setName(String name){
        this.name.set(name);
    }
    /**
     * sets the text of the tweet
     * @param text 
     */
    public void setText(String text){
        this.text.set(text);
    }
    /**
     * sets the imageURL profile pic of the user of the tweet
     * @param imageURL 
     */
    public void setImageURL(String imageURL){
        this.imageURL.set(imageURL);
    }
    /**
     * sets the handle of the user that tweeted the tweet
     * @param handle
     */
    public void setHandle(String handle){
        this.handle.set(handle);
    }
    /**
     * sets the tweetID of the tweet
     * @param tweetID 
     */
    public void setTweetID(long tweetID){
        this.tweetID.set(tweetID);
    }
    /**
     * returns the javafx property of the name of the user
     * @return StringProperty
     */
    public StringProperty nameProperty(){
        return this.name;
    }
    /**
     * returns the javafx property of the text of the tweet
     * @return StringProperty
     */
    public StringProperty textProperty(){
        return this.text;
    }
    /**
     * returns the javafx property of the imageURL of the user profile picture
     * @return StringProperty
     */
    public StringProperty imageURLProperty(){
        return this.imageURL;
    }
    /**
     * returns the javafx property of the handle of the tweet user
     * @return StringProperty
     */
    public StringProperty handleProperty(){
        return this.handle;
    }
    /**
     * returns the javafx property of the tweetID
     * @return LongProperty
     */
    public LongProperty tweetIDProperty(){
        return this.tweetID;
    }
    
    /**
     * creates hash code with all class variables
     * @return 
     */
    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.name.get() == null)? 0 : this.name.get().hashCode());
        result = prime * result + ((this.text.get() == null)? 0 : this.text.get().hashCode());
        result = prime * result + ((this.imageURL.get() == null)? 0: this.imageURL.get().hashCode());
        result = prime * result + ((this.handle.get() == null)? 0: this.handle.get().hashCode());
        return result;
    }

    /**
     * compares the two object to see 
     * if they are the same type
     * and all their class variable match
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DatabaseTweet other = (DatabaseTweet) obj;
        if (!Objects.equals(this.name.get(), other.name.get())) {
            return false;
        }
        if (!Objects.equals(this.text.get(), other.text.get())) {
            return false;
        }
        if (!Objects.equals(this.imageURL.get(), other.imageURL.get())) {
            return false;
        }
        if (!Objects.equals(this.handle.get(), other.handle.get())) {
            return false;
        }
        return true;
    }
}
