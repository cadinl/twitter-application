
package com.cadinlondono.twitterApplication.business.tweetBeans;

/**
 *This is the tweet interface that
 * the timeline will use to keep the different 
 * types of tweets in the application
 * usable to display the timeline
 * @author Cadin Londono
 */
public interface Tweet {
    
    public String getName();
    public String getText();
    public String getImageURL();
    public String getHandle();
    public long getTweetID();
}
