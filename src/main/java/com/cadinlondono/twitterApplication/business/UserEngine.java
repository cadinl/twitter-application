
package com.cadinlondono.twitterApplication.business;

import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.Relationship;


/**
 * This engine is part of the engines that use
 * twitter4j to provide the essential information
 * to this twitter application.
 * The user engine provides functionality related to action
 * we want to do with a user on twitter
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class UserEngine {
    
    private User user;
    private Twitter twitterInstance; 
    private TwitterEngine twitterEngine;
    
    /**
     * this constructor is used if we want to wrap 
     * a user object with the functionality of the app 
     * @param userId
     * @throws TwitterException 
     */
    public UserEngine(long userId) throws TwitterException
    {
        twitterInstance = getTwitterInstance();
        user = getUserById(userId);
        this.twitterEngine = new TwitterEngine();
    }
    /**
     * this constructor is used if we want to access general
     * functions for a user
     * @throws TwitterException 
     */
    public UserEngine() throws TwitterException
    {
        twitterInstance = getTwitterInstance();
        this.twitterEngine = new TwitterEngine();
    }
    
    private Twitter getTwitterInstance() {
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }
    /**
     * returns the user provided in constructor
     * @return User
     */
    public User getUser(){
        return this.user;
    }
    /**
     * returns an user object with the given user id
     * @param userId
     * @return User
     * @throws TwitterException 
     */
    public User getUserById(long userId)  throws TwitterException{
        if(this.twitterInstance == null)
        {
            this.twitterInstance = getTwitterInstance();
        }

        User user = twitterInstance.showUser(userId);
        return user;
        
    }
    /**
     * returns the user in that has been given in constructor
     * description
     * @return
     * @throws TwitterException 
     */
    public String getUserDescription() throws TwitterException{
        String description = user.getDescription();
        return description;
    }
    /**
     * gets the user provided in constructor profile picture
     * @return
     * @throws TwitterException 
     */
    public String getUserProfilePictureURL() throws TwitterException{
        String url = user.getProfileImageURL();
        return url;
    }
    /**
     * checks if the user provided in constructor follows the user provided in method
     * @param otherUserId
     * @return  boolean
     * @throws TwitterException 
     */
    public boolean doesUserFollowUser(long otherUserId) throws TwitterException{
        Relationship relationship = this.twitterInstance.showFriendship(user.getId(), otherUserId);
        return relationship.isTargetFollowedBySource();
    }
    /**
     * checks if the user provided in method follows the user provided in constructor 
     * @param otherUserId
     * @return  boolean
     * @throws TwitterException 
     */
    public boolean isOtherUserFollowingUser(long otherUserId) throws TwitterException{
        Relationship relationship = this.twitterInstance.showFriendship(user.getId(), otherUserId);
        return relationship.isSourceFollowedByTarget();
    }
    /**
     * makes the logged in user follow the user provided in method
     * @param otherUserId
     * @throws TwitterException 
     */
    public void followUser(long otherUserId) throws TwitterException{
        this.twitterInstance.createFriendship(otherUserId);
    }
    /**
     * makes the logged in user unfollow the user provided in method
     * @param otherUserId
     * @throws TwitterException 
     */
    public void unfollowUser(long otherUserId) throws TwitterException{
        this.twitterInstance.destroyFriendship(otherUserId);
    }
    /**
     * returns the number of followers of user provided in constructor
     * @return
     * @throws TwitterException 
     */
    public int getNumberOfFollowes() throws TwitterException{
        return this.user.getFollowersCount();
    }
    /**
     * returns the number of people the user provided in constructor is following
     * @return
     * @throws TwitterException 
     */
    public int getNumberOfFollowing() throws TwitterException{
        return this.user.getFriendsCount();
    }
    /**
     * sends direct message to user provided in constructor
     * @param message
     * @throws TwitterException 
     */
    public void sendDirectMessage(String message) throws TwitterException {
       this.twitterInstance.sendDirectMessage(this.user.getId(), message);
    }
    /**
     * gets the logged in user of the twitter session
     * @return User
     * @throws TwitterException 
     */
    public User getLoggedInUser() throws TwitterException {
        return this.twitterEngine.getMainUser();
    }
}
