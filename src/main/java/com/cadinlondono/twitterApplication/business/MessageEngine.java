
package com.cadinlondono.twitterApplication.business;


import java.util.ArrayList;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.DirectMessageList;
import twitter4j.DirectMessage;
import twitter4j.User;
import java.util.List;
import java.util.stream.Collectors;
/**
 * This engine is part of the engines that use
 * twitter4j to provide the essential information
 * to this twitter application.
 * The message engine more specifically loads a list 
 * of messages for different needs
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class MessageEngine {
    
    private Twitter twitterInstance;
    private DirectMessageList directMessages;
    /**
     * Constructor that only gets the twitter singleton
     * and saves it in a class variable
     * @throws TwitterException 
     */
    public MessageEngine() throws TwitterException
    {
        this.twitterInstance = getTwitterInstance();
        loadDirectMessages();
    }
    /**
     * gets twitter instance from twitter4j singleton
     * @return Twitter
     */
    private Twitter getTwitterInstance() {
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;
    }
    /**
     * loads all direct messages that the logged in user
     * has received or sent.
     * (only loads up to 100 messages)
     * @throws TwitterException 
     */
    private void loadDirectMessages() throws TwitterException{
        if(this.twitterInstance == null)
        {
            this.twitterInstance = getTwitterInstance();
        }
        
        this.directMessages = this.twitterInstance.getDirectMessages(100);
    }
    /**
     * returns all the direct messages that has been loaded 
     * @return List of direct messages
     * @throws TwitterException 
     */
    public List<DirectMessage> getDirectMessages() throws TwitterException{
        List<DirectMessage> messagesList = this.directMessages;
        return messagesList;
    }
    /**
     * returns all the direct message that either have the userId provided
     * as receiver or recipient
     * @param userId
     * @return List of direct messages
     * @throws TwitterException 
     */
    public List<DirectMessage> getDirectMessagesByUser(long userId) throws TwitterException{
        List<DirectMessage> allMessages = this.directMessages;
        List<DirectMessage> messagesFromUser = allMessages.stream()
                                    .filter(message -> message.getRecipientId() == userId || message.getSenderId() == userId)
                                    .collect(Collectors.toList());
        
        return messagesFromUser;
    }
    /**
     * Returns a list of all the users that the logged in user
     * has interacted with
     * @return List of users
     * @throws TwitterException 
     */
    public List<User> allUsersThatUserHasInteractedWith() throws TwitterException{
        List<User> users = new ArrayList<User>();
        UserEngine userEngine = new UserEngine();
        List<DirectMessage> directMessages = getDirectMessages();
        
        for(DirectMessage dm : directMessages)
        {
            long userId = dm.getRecipientId();
            User recipientUser = userEngine.getUserById(userId);
            if(!users.contains(recipientUser))
            {
                users.add(recipientUser);
            }
            
            userId = dm.getRecipientId();
            User receiverUser = userEngine.getUserById(userId);
            if(!users.contains(receiverUser))
            {
                users.add(receiverUser);
            }
            
        }
        return users;
    }
}
