package com.cadinlondono.twitterApplication.business;

import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import com.cadinlondono.twitterApplication.business.tweetBeans.TwitterInfo;
import com.cadinlondono.twitterApplication.persistance.TweetsDAO;
import com.cadinlondono.twitterApplication.persistance.TweetsDAOImplementation;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import java.util.ArrayList;

/**
 * This engine is part of the engines that use
 * twitter4j to provide the essential information
 * to this twitter application.
 * The timeline engine more specifically loads different
 * types of timelines of statuses that we
 * need for the application
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class TimelineEngine {
    
    /**
     * returns the twitter instance object 
     * @return Twitter
     */
    public Twitter getTwitterInstance() {
        Twitter twitter = TwitterFactory.getSingleton();
        return twitter;

    }
    /**
     * returns the home timeline of the logged in users
     * @param page
     * @param count
     * @return List of tweets
     * @throws TwitterException 
     */
    public List<Tweet> getHomeTimeLine(int page, int count) throws TwitterException {
        Paging paging = new Paging(page, count);
        Twitter twitter = getTwitterInstance();
        List<Status> statuses = twitter.getHomeTimeline(paging);
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            tweets.add(new TwitterInfo(status));
        }
        
        return tweets;
    }
    /**
     * returns the specified user profile timeline
     * @param screenName
     * @param page
     * @param count
     * @return List of tweets
     * @throws TwitterException 
     */
    public List<Tweet> getUserTimeline(String screenName, int page, int count) throws TwitterException {
        Twitter twitter = getTwitterInstance();
        Paging paging = new Paging(page, count);
        List<Status> statuses = twitter.getUserTimeline(screenName, paging);
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            tweets.add(new TwitterInfo(status));
        }
        
        return tweets;
    }
    /**
     * returns all the tweets that the logged in users
     * has been mentioned in
     * @param page
     * @param count
     * @return List of tweets
     * @throws TwitterException 
     */
    public List<Tweet> getMentionsTimeline(int page, int count) throws TwitterException{
        Paging paging = new Paging(page, count);
        Twitter twitter = getTwitterInstance();
        List<Status> statuses = twitter.getMentionsTimeline(paging);
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            tweets.add(new TwitterInfo(status));
        }
        
        return tweets;

    }
    /**
     * returns all the tweets related to the search term
     * specified
     * @param search
     * @param page
     * @param count
     * @return list of tweets
     * @throws TwitterException 
     */
    public List<Tweet> getSearchTimeline(String search, int page, int count) throws TwitterException{
        Paging paging = new Paging(page, count);
        Twitter twitter = getTwitterInstance();
        Query query = new Query(search);
        QueryResult queryResult = twitter.search(query);
        List<Status> statuses = queryResult.getTweets();
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            tweets.add(new TwitterInfo(status));
        }
        
        return tweets;
    }
    /**
     * gets a list of tweets from the database
     * that the user saved
     * @return List of tweets
     * @throws SQLException
     * @throws IOException 
     */
    public List<Tweet> getBookmarkedTimeline() throws SQLException, IOException{
        TweetsDAO tweetsDAO = new TweetsDAOImplementation();
        return tweetsDAO.findAll();
    }
    
    /**
     * gets a list of all the tweets that the user has retweeted
     * @return List of Tweets
     * @throws TwitterException 
     */
    public List<Tweet> getUserRetweets() throws TwitterException{
        Twitter twitter = getTwitterInstance();
        String screenName = twitter.showUser(twitter.getId()).getScreenName();
        List<Status> statuses = twitter.getUserTimeline(screenName);
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            if(status.isRetweet())
            {
                tweets.add(new TwitterInfo(status));
            }
        }
        
        return tweets;
    }
    
    /**
     * returns a list of retweets of a tweet the user made
     * @return List of tweets
     * @throws TwitterException 
     */
    public List<Tweet> getUserTweetsRetweeted() throws TwitterException{
        Twitter twitter = getTwitterInstance();
        String screenName = twitter.showUser(twitter.getId()).getScreenName();
        List<Status> statuses = twitter.getUserTimeline(screenName);
        
        //converting the status objects into twitter info objects
        List<Tweet> tweets = new ArrayList();
        for(Status status : statuses)
        {
            List<Status> retweets = twitter.getRetweets(status.getId());
            for(Status retweet : retweets)
            {
                tweets.add(new TwitterInfo(retweet));
            }
        }
        
        return tweets;
    }
    
    
}
