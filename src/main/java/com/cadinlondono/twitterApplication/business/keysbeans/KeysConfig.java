
package com.cadinlondono.twitterApplication.business.keysbeans;

import java.util.Objects;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
/**
 * property bean
 * based on https://gitlab.com/omniprof/javafx_properties_demo
 * @author cadin
 */
public class KeysConfig {
    
    private final StringProperty consumerKey;
    private final StringProperty consumerSecret;
    private final StringProperty accessToken;
    private final StringProperty accessTokenSecret;
    
    public KeysConfig(){
        this("","","","");
    }
    /**
     * 
     * @param consumerKey
     * @param consumerSecret
     * @param accessToken
     * @param accessTokenSecret 
     */
    public KeysConfig(final String consumerKey, final String consumerSecret,final String accessToken, final String accessTokenSecret){
        super();
        this.consumerKey = new SimpleStringProperty(consumerKey);
        this.consumerSecret = new SimpleStringProperty(consumerSecret);
        this.accessToken = new SimpleStringProperty(accessToken);
        this.accessTokenSecret = new SimpleStringProperty(accessTokenSecret);
    }
    
    public String getConsumerKey(){
        return this.consumerKey.get();
    }
    
    public String getConsumerSecret(){
        return this.consumerSecret.get();
    }
    
    public String getAccessToken(){
        return this.accessToken.get();
    }
    
    public String getAccessTokenSecret(){
        return this.accessTokenSecret.get();
    }
    
    public void setConsumerKey(String consumerKey){
        this.consumerKey.set(consumerKey);
    }
    
    public void setConsumerSecret(String consumerSecret){
        this.consumerSecret.set(consumerSecret);
    }
    
    public void setAccessToken(String accessToken){
        this.accessToken.set(accessToken);
    }
    
    public void setAccessTokenSecret(String accessTokenSecret){
        this.accessTokenSecret.set(accessTokenSecret);
    }
    
    public StringProperty consumerKeyProperty(){
        return this.consumerKey;
    }
    
    public StringProperty consumerSecretProperty(){
        return this.consumerSecret;
    }
    
    public StringProperty accessTokenProperty(){
        return this.accessToken;
    }
    
    public StringProperty accessTokenSecretProperty(){
        return this.accessTokenSecret;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.consumerKey.get());
        hash = 17 * hash + Objects.hashCode(this.consumerSecret.get());
        hash = 17 * hash + Objects.hashCode(this.accessToken.get());
        hash = 17 * hash + Objects.hashCode(this.accessTokenSecret.get());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KeysConfig other = (KeysConfig) obj;
        if (!Objects.equals(this.consumerKey, other.consumerKey)) {
            return false;
        }
        if (!Objects.equals(this.consumerSecret, other.consumerSecret)) {
            return false;
        }
        if (!Objects.equals(this.accessToken, other.accessToken)) {
            return false;
        }
        if (!Objects.equals(this.accessTokenSecret, other.accessTokenSecret)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "KeysConfigProperties\n{\t" + "consumerKey=" + consumerKey.get() + "\n\tconsumerSecret=" + consumerSecret.get() + "\n\taccessToken=" + accessToken.get() + "\n\taccessToken=" + accessToken.get() +"\n}";
    }
}
