
package com.cadinlondono.twitterApplication.business.propertiesManager;

import com.cadinlondono.twitterApplication.business.keysbeans.KeysConfig;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import static java.nio.file.Files.newInputStream;
import static java.nio.file.Files.newOutputStream;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *This object is use to manipulate the beans
 * to load them and write to them from a file
 * based on https://gitlab.com/omniprof/javafx_properties_demo
 * @author cadin
 */
public class KeysPropertiesManager {
    
    private final static Logger LOG = LoggerFactory.getLogger(KeysPropertiesManager.class);
    /**
     * this method loads the properties of the twitter keys to a keys config bean
     * @param keysConfig
     * @param path
     * @param propFileName
     * @return
     * @throws IOException 
     */
    public final boolean loadKeysProperties(final KeysConfig keysConfig, final String path, final String propFileName) throws IOException {

        boolean found = false;
        Properties prop = new Properties();

        Path txtFile = get(path, propFileName + ".properties");

        // File must exist
        if (Files.exists(txtFile)) {
            try (InputStream propFileStream = newInputStream(txtFile);) {
                prop.load(propFileStream);
            }
            keysConfig.setConsumerKey(prop.getProperty("oauth.consumerKey"));
            keysConfig.setConsumerSecret(prop.getProperty("oauth.consumerSecret"));
            keysConfig.setAccessToken(prop.getProperty("oauth.accessToken"));
            keysConfig.setAccessTokenSecret(prop.getProperty("oauth.accessTokenSecret"));

            found = true;
        }
        return found;
    }
    /**
     * this method takes a keysConfig bean and writes is properties
     * to a properties file
     * @param path
     * @param propFileName
     * @param keysConfig
     * @throws IOException 
     */
    public final void writeKeysProperties(final String path, final String propFileName, final KeysConfig keysConfig) throws IOException {

        Properties prop = new Properties();

        prop.setProperty("oauth.consumerKey", keysConfig.getConsumerKey());
        prop.setProperty("oauth.consumerSecret", keysConfig.getConsumerSecret());
        prop.setProperty("oauth.accessToken", keysConfig.getAccessToken());
        prop.setProperty("oauth.accessTokenSecret", keysConfig.getAccessTokenSecret());
        
        Path txtFile = get(path, propFileName + ".properties");

        // Creates the file or if file exists it is truncated to length of zero
        // before writing
        try (OutputStream propFileStream = newOutputStream(txtFile)) {
            prop.store(propFileStream, "Twitter Keys Properties");
            LOG.debug("ARRIVE TO WRITE");
        }
    }
}
