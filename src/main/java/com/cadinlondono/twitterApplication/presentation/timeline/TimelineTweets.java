
package com.cadinlondono.twitterApplication.presentation.timeline;

import com.cadinlondono.twitterApplication.business.TimelineEngine;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import com.cadinlondono.twitterApplication.presentation.view.message.TweetFXMLController;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
/**
 *  Class that creates a list view depending 
 * on the type of timeline you want it to return;
 * 
 * @author cadin
 * @version 1.0
 */


public class TimelineTweets {
    
    private final static Logger LOG = LoggerFactory.getLogger(TimelineTweets.class);
    
    private final String TWEET_FILE_PATH = "/fxml/message/Tweet.fxml";//string to the tweet container
    
    private final TimelineEngine timelineEngine;
    private List<Tweet> rawTweets;
    private StackPane stackPane;
    private int page = 1;
    
    /**
     * default constructor
     * is use for the default timeline to be shown
     * @throws TwitterException 
     */
    public TimelineTweets() throws TwitterException{
        //retrieves the timeline information from twitter
        timelineEngine = new TimelineEngine();
        changeToHomeTimeline();
    }
    /**
     * changes the timeline to only have the tweets that the user
     * has been mentioned on
     * @throws TwitterException 
     */
    public void changeToMentions() throws TwitterException{
        rawTweets = timelineEngine.getMentionsTimeline(page, 20);
    }
    /**
     * changes the timeline to display tweets from the user with 
     * the screenName provided
     * @param screenName
     * @throws TwitterException 
     */
    //for user we only want to get 10 tweets to make it more faster
    public void changerToUserTimeline(String screenName) throws TwitterException{
        rawTweets = timelineEngine.getUserTimeline(screenName, page, 10);
    }
    /**
     * changes timeline to have tweets from the search term provided
     * @param search
     * @throws TwitterException 
     */
    public void changeToSearch(String search) throws TwitterException{
        rawTweets = timelineEngine.getSearchTimeline(search, page, 20);
    }
    /**
     * changes timeline to display tweets of the home timeline, of the
     * logged in user
     * @throws TwitterException 
     */
    public void changeToHomeTimeline() throws TwitterException{
        rawTweets = timelineEngine.getHomeTimeLine(page, 20);
    }
    /**
     * changes timeline to only have the tweets saved in database
     * @throws SQLException
     * @throws IOException 
     */
    public void changeToBookmarkedTimeline() throws SQLException, IOException{
        rawTweets = timelineEngine.getBookmarkedTimeline();
    }
    
    /**
     * changes timeline to have the tweets that others user has retweeted 
     * of the logged in user
     * @throws TwitterException 
     */
    public void changeUserTweetsRetweeted() throws TwitterException{
        rawTweets = timelineEngine.getUserTweetsRetweeted();
    }
    /**
     * changes timeline to have the tweets that the user has retweeted
     * @throws TwitterException 
     */
    public void changeToUserRetweets() throws TwitterException{
        rawTweets = timelineEngine.getUserRetweets();
    }
    
    /**
     * this method creates a list of VBoxes(tweets)
     * with the tweets that the raw tweet property has
     * @return Tweets
     * @throws TwitterException 
     */
    public List<VBox> getTimelineList() throws TwitterException, IOException
    {
        //create a list of vbox which will be visual representation of tweets
        List<VBox> timelineTweets = new ArrayList<>();
        //creates a vbox for every status and stores it into the list
        for(Tweet tweet: rawTweets)
        {
            try{
            VBox tweetBox = createTweet(tweet);
            timelineTweets.add(tweetBox);
            }
            catch(Exception e){
                LOG.error("couldn't load VBox");
                throw e;
            }
        }
        
        
        
        return timelineTweets;
        
    }
    /**
     * sets the stackPane class variable object of the class
     * to give tweets the ability to change to user profile
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    /**
     * sets the page of the timeline to be the next one.
     * the class that use it will then need to refresh the timeline
     */
    public void nextPage(){
        this.page++;
    }
    
    /**
     * this method with the given status will load the
     * the tweet FXML container and populate is info
     * with the status info
     * @param status
     * @return
     * @throws Exception 
     */
    private VBox createTweet(final Tweet tweet) throws IOException, TwitterException
    {      
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.TWEET_FILE_PATH));


        //loading the VBox
        VBox tweetContainer = (VBox) loader.load();
        //getting the controller
        TweetFXMLController controller = loader.getController();
        //setting all the content for the tweet box
        controller.setTweet(tweet);

        controller.setStackPane(this.stackPane);
        
        return tweetContainer; 
    }
    

    

    
}

