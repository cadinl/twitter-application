
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.presentation.timeline.TimelineTweets;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller uses a list view to display the general timeline
 * of the logged in user
 * 
 * Added the createTimeline method the sets the list view
 * with the list retrieved from the timeline tweets class
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class TimelinePageFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(TimelinePageFXMLController.class);
    
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="timeline"
    private ListView<VBox> timeline; // Value injected by FXMLLoader
    
    private TimelineTweets timelineTweets;// used to retrieve tweets
    private List<VBox> tweetList;//use to keep a list of tweets being displayed
    private StackPane stackPane;//use to change panes

    /**
     * this method is called automatically after
     * the FXML file is loaded
     * 
     * it calls the createTimeline method
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws TwitterException {
        assert timeline != null : "fx:id=\"timeline\" was not injected: check your FXML file 'TimelineListFXML.fxml'.";
        this.timelineTweets = new TimelineTweets();
    }
    /**
     * sets the StackPane to be able to switch panes
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    /**
     * refreshes the tweets by getting a new list of all the tweets
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshTimeline()throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList<VBox>();
        
        createTimeline();
    }
    
    /**
     * this method set the timeline list view property
     * by getting all the VBox items from the timeline tweets
     * class
     * @throws TwitterException 
     */
    public void createTimeline() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeToHomeTimeline();
            //if the new list is empty do not added to the original list
            //this means no more tweets can be gathered
            if(!timelineTweets.getTimelineList().isEmpty())
            {
            this.tweetList.addAll(timelineTweets.getTimelineList());
            addMoreButton(this.tweetList);
            }
            timeline.setItems(FXCollections.observableArrayList(this.tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    //this button allows to load more tweets using pagination
    private void addMoreButton(List<VBox> tweetList){
        VBox buttonPane = new VBox();
        Button moreButton = new Button(this.resources.getString("More"));
        moreButton.setOnAction(event -> moreButtonEvent());
        buttonPane.getChildren().add(moreButton);
        tweetList.add(buttonPane);
    }
    //more button event handler
    private void moreButtonEvent(){
        try
        {
            this.timelineTweets.nextPage();
            createTimeline();
        }
        catch(IOException ex)
        {
            LOG.error("couldn't load fxml of timeline next page", ex);
        }
        catch(TwitterException ex)
        {
            LOG.error("couldn't load timeline next page from twitter", ex);
        }
        
    }
}
