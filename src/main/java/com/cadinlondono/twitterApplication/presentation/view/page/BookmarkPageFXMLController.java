
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.presentation.timeline.TimelineTweets;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.sql.SQLException;
import javafx.scene.layout.StackPane;
import twitter4j.TwitterException;


/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller purpose is to shows a timeline with the bookmarked
 * tweets
 * 
 * Added createBookmarkList that creates list of 
 * tweets that the user has bookmarked
 * (doesn't show actual bookmarked tweets for now)
 * 
 * Added the create bookmarkedList this method
 * should be for part 2 and in another class
 * 
 * Added the createBookmarked method
 * this is for testing purposes only 
 * 
 * @author Cadin Londono
 * @version 1.0
 */

public class BookmarkPageFXMLController {
    
    //LOG object
    private final static Logger LOG = LoggerFactory.getLogger(NotificationPageFXMLController.class);

    private final String TWEET_FILE_PATH = "/fxml/message/Tweet.fxml";
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="borderPane"
    private BorderPane borderPane; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarkedTweets"
    private ListView<VBox> bookmarkedTweets; // Value injected by FXMLLoader
    
    private StackPane stackPane;//we need this so the tweets can change to show user profile
    
    private TimelineTweets timelineTweets;

    /**
     * this method is called automatically after
     * the FXML file is loaded
     * 
     * it sets the bookmark ListView
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException, TwitterException {
        assert borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'bookmarkPageFXML.fxml'.";
        assert bookmarkedTweets != null : "fx:id=\"bookmarkedTweets\" was not injected: check your FXML file 'bookmarkPageFXML.fxml'.";

        this.timelineTweets = new TimelineTweets();
    }
    
    public void setStackPane(StackPane stackPane){
        this.stackPane = stackPane;
    }
    
    /**
     * this method sets the list view content
     * with the list provided by timelineTweets
     * method with all the tweets saved in the database
     * @throws IOException 
     */
    public void setBookmarkedList() throws TwitterException, IOException{
       try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeToBookmarkedTimeline();
            this.bookmarkedTweets.setItems(FXCollections.observableArrayList(this.timelineTweets.getTimelineList()));
        }
        catch(SQLException ex){
            LOG.error("couldn't retrieve timeline from database", ex);
            //i do not throw the exception again in case the user has no mysql database
            //we still want him to use the app but the listview will just be empty 
        }
       //throws twitterException because once the tweet is loaded from db
       //i add all the extra information from the twitterAPI
       //like show user profile, retweet, like, etc ...
        catch(TwitterException ex){
           LOG.error("couldn't retrieve information from twitter for the tweets stored in db", ex);
           throw ex;
        }
       catch(IOException ex){
           LOG.error("couldn't load the tweet fxml",ex);
           throw ex;
       }
    }

}
