
package com.cadinlondono.twitterApplication.presentation.view.message;

import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.Node;
import java.net.URL;
import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import com.cadinlondono.twitterApplication.constants.TwitterConstants;
import com.cadinlondono.twitterApplication.business.TweetEngine;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import com.cadinlondono.twitterApplication.persistance.TweetsDAO;
import com.cadinlondono.twitterApplication.persistance.TweetsDAOImplementation;
import com.cadinlondono.twitterApplication.presentation.alert.CommentDialogController;
import com.cadinlondono.twitterApplication.presentation.alert.ErrorAlert;
import com.cadinlondono.twitterApplication.presentation.alert.RetweetDialogController;
import com.cadinlondono.twitterApplication.presentation.view.page.ProfilePageFXMLController;
import java.io.IOException;
import java.sql.SQLException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This the container for a tweet
 * that will be displayed in a list view
 * 
 * every tweet has 4 buttons that can be activated
 * and will do an action depending on the user that has tweeted
 * or the specific tweet
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class TweetFXMLController {
    //LOG object
    private final static Logger LOG = LoggerFactory.getLogger(TweetFXMLController.class);
    
    //needed to open popup dialog
    private final String COMMENT_FILE_PATH = "/fxml/utils/commentDialog.fxml";
    private final String RETWEET_FILE_PATH = "/fxml/utils/retweetDialog.fxml";

    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="profilePicture"
    private ImageView profilePicture; // Value injected by FXMLLoader

    @FXML // fx:id="userButton"
    private Button userButton; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="tweetArea"
    private Label tweetArea; // Value injected by FXMLLoader

    @FXML // fx:id="comment"
    private ImageView comment; // Value injected by FXMLLoader

    @FXML // fx:id="retweet"
    private ImageView retweet; // Value injected by FXMLLoader

    @FXML // fx:id="like"
    private ImageView like; // Value injected by FXMLLoader

    @FXML // fx:id="more"
    private ImageView more; // Value injected by FXMLLoader
    
    @FXML // fx:id="numberOfRetweets"
    private Label numberOfRetweets; // Value injected by FXMLLoader
    
    @FXML // fx:id="numberOfLikes"
    private Label numberOfLikes; // Value injected by FXMLLoader
    
    @FXML // fx:id="save"
    private ImageView save; // Value injected by FXMLLoader
    
    private Tweet tweet;//based tweet object
    private TweetEngine tweetEngine;//used to add functionality to the tweet like: like,retweet, etc...
    private StackPane stackPane;//use to add the functionality to the tweet to view the user of the tweet, profile
    
    private TweetsDAO tweetsDAO;//access database
    
    private boolean tweetAlreadySaved;//boolean that indicates if tweet already saved in db
    
    private Image savedTweet, unSavedTweet;//images that we load at the start for the db storing of the tweet

    /**
     * this is the event handler for the comment button.
     * It opens up the comment dialog
     * @param event 
     */
    @FXML
    void commentOnTweet(MouseEvent event) throws IOException, TwitterException {
        LOG.info("clicked the commented button of the tweet");
        openCommentDialog();
    }
     /**
     * this is the event handler for the like button.
     * It also now visually changes the number of likes
     * @param event 
     */
    @FXML
    void likeTweet(MouseEvent event) {
        LOG.info("You have liked this tweet");
        try
        {
            if(tweetEngine.isFavorite())
            {
                this.tweetEngine.unfavoriteTweet();
                //subtracting 1 like visually
                this.numberOfLikes.setText((Integer.parseInt(this.numberOfLikes.getText()) - 1 )+"");
            }
            else
            {
                this.tweetEngine.favoriteTweet();
                //adding 1 like visually
                this.numberOfLikes.setText((Integer.parseInt(this.numberOfLikes.getText()) + 1 )+"");
            }
        }
        catch(TwitterException e)
        {
            LOG.error("couldn't like a tweet", e);
        }
    }
     /**
     * this is the event handler for the retweet button. opens up
     * the retweet dialog window
     * @param event 
     */
    @FXML
    void retweetTweet(MouseEvent event) throws IOException, TwitterException {
        LOG.info("clicked on retweet");
        openRetweetDialog();
    }
     /**
     * this is the event handler for the user handle button
     * @param event 
     */
    @FXML
    void showUser(ActionEvent event) throws TwitterException{
            FXMLLoader loader = new FXMLLoader();
            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(this.getClass().getResource(TwitterConstants.PROFILE_FXML_PATH));
            //the same resource for every page
            loader.setResources(ResourceBundle.getBundle(TwitterConstants.TWITTER_BUNDLE));
            
            try
            {   
                Pane pane = (Pane) loader.load();
                
                ProfilePageFXMLController profileController = loader.getController();
                profileController.setStackPane(stackPane);
                profileController.setUser(this.tweetEngine.getUser());
                profileController.refreshTimeline();
                
                hideBackgroundPane();
                
                this.stackPane.getChildren().add(pane);
            }
            catch(IOException e)
            {
                LOG.error("couldn't load user page", e);
            }
    }
    /**
     * this method checks if the tweet has already been saved by the database
     * and if its not, it will be saved in the db. If it is
     * already in the database it will be deleted
     * @param event 
     */
    @FXML
    void saveTweet(MouseEvent event) {
        try
        {
            if(this.tweetAlreadySaved)
            {
                tweetsDAO.deleteTweet(this.tweet.getTweetID());
                LOG.info("unsave tweet succesfully in the database");
                this.tweetAlreadySaved = false;
                this.save.setImage(this.unSavedTweet);
            }
            else
            {
                tweetsDAO.createTweet(this.tweet);
                LOG.info("saved succesfully the tweet into the database");
                this.tweetAlreadySaved = true;
                this.save.setImage(this.savedTweet);
            }
        }
        catch(SQLException e)
        {
            //if there was a database connection issue
            LOG.error( "couldn't save tweet",e);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("couldn't save the tweet in the database (SQL Exception)");
        }

    }

     /**
     * this method is called automatically after
     * the FXML file is loaded
     * 
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException {
        assert profilePicture != null : "fx:id=\"profilePicture\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert userButton != null : "fx:id=\"userButton\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert username != null : "fx:id=\"username\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert tweetArea != null : "fx:id=\"tweetArea\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert comment != null : "fx:id=\"commen\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert retweet != null : "fx:id=\"retweet\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert like != null : "fx:id=\"like\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert more != null : "fx:id=\"more\" was not injected: check your FXML file 'TweetFXML.fxml'.";
        assert save != null : "fx:id=\"save\" was not injected: check your FXML file 'Tweet.fxml'.";
        
        tweetsDAO = new TweetsDAOImplementation();
    }
    /**
     * sets the status that this tweet fxml will contain
     * @param status Status
     * @throws TwitterException 
     */
    public void setTweet(Tweet tweet) throws TwitterException
    {
        this.tweet = tweet;
        this.tweetEngine = new TweetEngine(this.tweet);
        setTweetContent();
    }
    /**
     * sets the StackPane to be able to change panes
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    

    /**
     * this method is use to create
     * an example of a bookmarked tweet.
     * used by the bookmark page control
     */
    public void setBookmarkedContent(){
        userButton.setText("not_a_real_user");
        username.setText("not_a_real_user");
        tweetArea.setText("this is a sample of a bookmarked tweet");
    }
    
     /**
     * this method with the status fills out the info
     * necessary to display the tweet
     * @param status 
     */
    private void setTweetContent() throws TwitterException
    {
        //getting all the information needed to populate a tweet
        userButton.setText(this.tweet.getHandle());
        username.setText(this.tweet.getName());
        tweetArea.setText(this.tweet.getText());
        
        //getting the profile picture of username from url 
        profilePicture.setImage(new Image(this.tweet.getImageURL()));
        
        numberOfLikes.setText(tweetEngine.getFavoriteCount() + "");
        numberOfRetweets.setText(tweetEngine.getRetweetCount() + "");
        
        loadImages();
        this.tweetAlreadySaved = checkIfTweetIsAlreadySaved(this.tweet);
       
    }
    //loads the images to change the icon of the saved tweet
    private void loadImages(){ 
        this.unSavedTweet = new Image("images/BookmarksButton.png");
        this.savedTweet = new Image("images/BookmarkedButton.png");
    }
    
    //used to see if we want to unsave or save the tweet
    private boolean checkIfTweetIsAlreadySaved(Tweet tweet){
        try
        {
            if(tweetsDAO.isTweetAlreadySaved(this.tweet.getTweetID()))
            {
                this.save.setImage(this.savedTweet);
                return true;
            }
            else
            {
                //by default the tweet has the not saved button image
                //so no need to change it
                return false;
            }
        }
        catch(SQLException e)
        {
            LOG.error( "couldn't access Database",e);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("couldn't retrieve information from MySQL database");
        }
        //if there was an error we want to return false 
        return false;
    }
    //opens the comment dialog for the user to write his comment
    private void openCommentDialog() throws IOException, TwitterException{
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.COMMENT_FILE_PATH));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use TwitterBundle.properties
       loader.setResources(ResourceBundle.getBundle("TwitterBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (VBox) loader.load();

        //getting the controller to set tweet
        CommentDialogController commentController = loader.getController();
        commentController.setTweet(this.tweet);
        
        //set the root as the scene and return it
        Scene scene = new Scene(root);
        
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.showAndWait(); 
        
    }
    //opens up a retweet dialog for the user to retweet or retweet with commetn
    private void openRetweetDialog()  throws IOException, TwitterException{
         // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.RETWEET_FILE_PATH));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use TwitterBundle.properties
       loader.setResources(ResourceBundle.getBundle("TwitterBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (VBox) loader.load();

        //getting the controller to set tweet
        RetweetDialogController retweetController = loader.getController();
        retweetController.setTweet(this.tweet);
        
        //set the root as the scene and return it
        Scene scene = new Scene(root);
        
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.showAndWait();     
    }
    
    //used when changing to profile pane when the user wishes to 
    //see the profile of the user that tweeted the tweet
    private void hideBackgroundPane(){
        Node currentPane = this.stackPane.getChildren().get(this.stackPane.getChildren().size()-1);
        currentPane.setVisible(false);
    }
}
