
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.presentation.timeline.TimelineTweets;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller purpose is to show the most popular hashtags
 * in twitter and to search for a tweet in twitter
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class ExplorePageFXMLController {
    
    private final static Logger LOG = LoggerFactory.getLogger(ExplorePageFXMLController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="searchBar"
    private TextField searchBar; // Value injected by FXMLLoader

    @FXML // fx:id="search"
    private Button search; // Value injected by FXMLLoader

    @FXML // fx:id="exploreList"
    private ListView<VBox> exploreList; // Value injected by FXMLLoader
    
    private StackPane stackPane;
    private TimelineTweets timelineTweets;
    private List<VBox> tweetList;
    private String searchTerm;

    /**
     * this method is called when the search button is pressed
     * it will retrieve the content from the search bar
     * and search it on twitter
     * the results will be displayed in the list view
     * @param event 
     */
    @FXML
    void searchTweet(ActionEvent event) throws TwitterException, IOException {
       LOG.info("you have searched for a tweet with this message: " + searchBar.getText());
       this.searchTerm = searchBar.getText();
       refreshTimeline();
       searchBar.setText("");
    }

    /**
     * this method is called automatically after
     * the FXML file is loaded
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert searchBar != null : "fx:id=\"searchBar\" was not injected: check your FXML file 'ExplorePageFXML.fxml'.";
        assert search != null : "fx:id=\"search\" was not injected: check your FXML file 'ExplorePageFXML.fxml'.";
        assert exploreList != null : "fx:id=\"exploreList\" was not injected: check your FXML file 'ExplorePageFXML.fxml'.";

    }
    /**
     * sets the StackPane to be able to change panes
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    /**
     * retrieves a new timeline from twitter, depending on the search
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshTimeline()throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList();
        createTimeline();
    }
    
    /**
     * this method set the timeline list view property
     * by getting all the VBox items from the timeline tweets
     * class
     * @throws TwitterException 
     */
    public void createTimeline() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeToSearch(this.searchTerm);
            if(!timelineTweets.getTimelineList().isEmpty())
            {
                this.tweetList.addAll(timelineTweets.getTimelineList());
                addMoreButton(tweetList);
            }
            exploreList.setItems(FXCollections.observableArrayList(tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    //this method adds the functionality to add more tweets with pagination
    private void addMoreButton(List<VBox> tweetList){
        VBox buttonPane = new VBox();
        Button moreButton = new Button(this.resources.getString("More"));
        moreButton.setOnAction(event -> moreButtonEvent());
        buttonPane.getChildren().add(moreButton);
        tweetList.add(buttonPane);
    }
    //this event with the timelinetweets will add the next page of tweets
    //to the list view
    private void moreButtonEvent(){
        try
        {
            this.timelineTweets.nextPage();
            createTimeline();
        }
        catch(IOException ex)
        {
            LOG.error("couldn't load fxml of timeline next page", ex);
        }
        catch(TwitterException ex)
        {
            LOG.error("couldn't load timeline next page from twitter", ex);
        }
        
    }
    
    
    
}
