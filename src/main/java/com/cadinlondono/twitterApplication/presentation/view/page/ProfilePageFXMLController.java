
package com.cadinlondono.twitterApplication.presentation.view.page;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.User;
import twitter4j.TwitterException;
import com.cadinlondono.twitterApplication.business.UserEngine;
import com.cadinlondono.twitterApplication.constants.TwitterConstants;
import com.cadinlondono.twitterApplication.presentation.directMessages.DirectMessageUtilityFXMLController;
import com.cadinlondono.twitterApplication.presentation.timeline.TimelineTweets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 *The user property needs to be set by the 
 * set user method in order for the controller
 * to fill the FXML with the user content
 * 
 * Added the setUser method to set the user property
 * 
 * Added the setFollowButtonVisible method to turn off the 
 * follow button if its own user profile
 * 
 * @author Cadin Londono
 * @version 1.0
 */

public class ProfilePageFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(ProfilePageFXMLController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="profileImage"
    private ImageView profileImage; // Value injected by FXMLLoader

    @FXML // fx:id="username"
    private Label username; // Value injected by FXMLLoader

    @FXML // fx:id="followingCount"
    private Label followingCount; // Value injected by FXMLLoader

    @FXML // fx:id="followersCount"
    private Label followersCount; // Value injected by FXMLLoader

    @FXML // fx:id="follow"
    private Button follow; // Value injected by FXMLLoader
    
    @FXML // fx:id="message"
    private Button message; // Value injected by FXMLLoader
    
    @FXML // fx:id="timeline"
    private ListView<VBox> timeline; // Value injected by FXMLLoader
    
    @FXML
    private StackPane stackPane;
    //the profile page of this user
    private User user;
    //user engine to get functionality
    private UserEngine userEngine;
    //if true the logged in user follows this user
    private boolean isFollowing;
    //use to display tweets by user
    private TimelineTweets timelineTweets;
    //list of all the tweets by the user
    private List<VBox> tweetList;
    

    /**
     * When the user clicks on the button he follows or unfollows the user
     * depending if he already follows him or not
     * (this is deactivated if its the own user profile)
     * @param event 
     */
    @FXML
    void followUnfollowUser(ActionEvent event) throws TwitterException {
        LOG.info("you have followed the user, if u have already followed then you have unfollowed");
        if(isFollowing)
        {
            userEngine.unfollowUser(this.user.getId());
            setFollowButton();
        }
        else
        {
            userEngine.followUser(this.user.getId());
            setFollowButton();
        }
    }
    
    @FXML
    void sendMessage(ActionEvent event) {
        loadDirectMessageFXML();
    }

    /**
     * this method is called automatically after
     * the FXML file is loaded
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert profileImage != null : "fx:id=\"profileImage\" was not injected: check your FXML file 'profilePageFXML.fxml'.";
        assert username != null : "fx:id=\"username\" was not injected: check your FXML file 'profilePageFXML.fxml'.";
        assert followingCount != null : "fx:id=\"followingCount\" was not injected: check your FXML file 'profilePageFXML.fxml'.";
        assert followersCount != null : "fx:id=\"followersCount\" was not injected: check your FXML file 'profilePageFXML.fxml'.";
        assert follow != null : "fx:id=\"follow\" was not injected: check your FXML file 'profilePageFXML.fxml'.";
        assert message != null : "fx:id=\"message\" was not injected: check your FXML file 'ProfilePage.fxml'.";
    }
    
    /**
     * This method is used to set the user property of the controller
     * @param user 
     */
    public void setUser(User user) throws TwitterException, IOException
    {
        this.user = user;
        this.userEngine = new UserEngine(user.getId());
        setUserContent();
    }
    /**
     * This method is used to set the follow button
     * invisible in the case if its the current user profile
     * (logged in user)
     */
    public void setFollowButtonInvsible()
    {
        this.follow.setDisable(true);
        this.follow.setVisible(false);
    }
    /**
     * sets the stackpane to be able to change panes
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    
    /**
     * this method is called by the setUser
     * Method and sets all the info needed in the page
     * with the user property info
     */
    private void setUserContent() throws TwitterException, IOException
    {
        if(this.user != null)//checking if this method wasn't called before the setUser method
        {
            Image image = new Image(this.user.getProfileImageURL());
            this.profileImage.setImage(image);
            this.username.setText(this.user.getName());
            this.followersCount.setText(this.user.getFollowersCount() + "");
            this.followingCount.setText(this.user.getFriendsCount() + "");
            
            setFollowButton();
            refreshTimeline();
        }
    }
    /**
     * refreshes the timeline with new tweets if they have been added
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshTimeline()throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList();
        createTimeline();
    }
    
    /**
     * this method set the timeline list view property
     * by getting all the VBox items from the timeline tweets
     * class
     * @throws TwitterException 
     */
    public void createTimeline() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changerToUserTimeline(user.getScreenName());
            if(!timelineTweets.getTimelineList().isEmpty())
            {
                this.tweetList.addAll(timelineTweets.getTimelineList());
                addMoreButton(tweetList);
            }
            timeline.setItems(FXCollections.observableArrayList(tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    //button use to get more tweets by user using pagination
    private void addMoreButton(List<VBox> tweetList){
        VBox buttonPane = new VBox();
        Button moreButton = new Button(this.resources.getString("More"));
        moreButton.setOnAction(event -> moreButtonEvent());
        buttonPane.getChildren().add(moreButton);
        tweetList.add(buttonPane);
    }
    //event handler for the more tweets button
    private void moreButtonEvent(){
        try
        {
            this.timelineTweets.nextPage();
            createTimeline();
        }
        catch(IOException ex)
        {
            LOG.error("couldn't load fxml of timeline next page", ex);
        }
        catch(TwitterException ex)
        {
            LOG.error("couldn't load timeline next page from twitter", ex);
        }
        
    }
    //sets the follow button depending of the relationship this user has with
    //the user profile is displaying
    private void setFollowButton() throws TwitterException {
       if(this.userEngine.isOtherUserFollowingUser(this.userEngine.getLoggedInUser().getId()))
       {
           this.isFollowing = true;
           this.follow.setText(this.resources.getString("Unfollow"));
       }
       else
       {
           this.isFollowing = false;
           this.follow.setText(this.resources.getString("Follow"));
       }
    }
    //Loads the direct message pane to send a direct message only to this user
    private void loadDirectMessageFXML(){
        FXMLLoader loader = new FXMLLoader();
            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(this.getClass().getResource(TwitterConstants.DM_UTILITY_FXML_PATH));
            //the same resource for every page
            loader.setResources(ResourceBundle.getBundle(TwitterConstants.TWITTER_BUNDLE));
            
            try
            {   
                Pane pane = (Pane) loader.load();
                
                DirectMessageUtilityFXMLController directUtilityController = loader.getController();
                directUtilityController.setStackPane(stackPane);
                directUtilityController.setUser(this.user);
                
                hideBackgroundPane();
                
                this.stackPane.getChildren().add(pane);
            }
            catch(IOException e)
            {
                LOG.error("couldn't load user page", e);
            }
    }
    
    //hides background pane if needed
    private void hideBackgroundPane(){
        Node currentPane = this.stackPane.getChildren().get(this.stackPane.getChildren().size()-1);
        currentPane.setVisible(false);
    }
    
}
