

package com.cadinlondono.twitterApplication.presentation.view.message;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

/**
 * THIS FUNCTIONALITY HASN'T BEEN COMPLETED
 * This the container for the notification info
 * that will be displayed in a list view
 * 
 * for a sample use i added the setNotificationContent
 * and the setMentionContent that adds as an example some
 * information
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class NotificationFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="profilePicture"
    private ImageView profilePicture; // Value injected by FXMLLoader

    @FXML // fx:id="notificationTitle"
    private Label notificationTitle; // Value injected by FXMLLoader

    @FXML // fx:id="notificationContent"
    private Label notificationContent; // Value injected by FXMLLoader

    /**
     * this method is called automatically after
     * the FXML file is loaded
     * 
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert profilePicture != null : "fx:id=\"profilePicture\" was not injected: check your FXML file 'NotificationFXML.fxml'.";
        assert notificationTitle != null : "fx:id=\"notificationTitle\" was not injected: check your FXML file 'NotificationFXML.fxml'.";
        assert notificationContent != null : "fx:id=\"notificationContent\" was not injected: check your FXML file 'NotificationFXML.fxml'.";

    }
    /**
     * sets the notification content for now
     * to an example of a notification
     */
    public void setNotificationContent(){
        notificationTitle.setText("This is a general notification");
        notificationContent.setText("The notification content goes here");
    }
    /**
     * sets the mention content for now
     * to an example of a mention
     */
    public void setMentionContent(){
        notificationTitle.setText("This is a mention");
        notificationContent.setText("The mention content goes here");
    }
}
