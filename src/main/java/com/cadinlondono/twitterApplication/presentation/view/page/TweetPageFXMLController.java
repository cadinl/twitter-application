
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.business.TwitterEngine;
import com.cadinlondono.twitterApplication.presentation.alert.ErrorAlert;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;


/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller only purpose is to create a tweet with 
 * the message that the user typed in
 * 
 * Added 
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class TweetPageFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(TweetPageFXMLController.class);
    
    private final int TWITTER_TWEET_LIMIT = 280; //the twitter tweet character limit
    
    private final TwitterEngine twitterEngine = new TwitterEngine();
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="tweetMessage"
    private TextArea tweetMessage; // Value injected by FXMLLoader

    @FXML // fx:id="send"
    private Button send; // Value injected by FXMLLoader

    /**
     * this method is called when the user clicks on the 
     * send button 
     * it checks if the tweet doesn't exceed the character limit
     * with the helper method 
     * and sends the tweet
     * @param event
     * @throws TwitterException 
     */
    @FXML
    void sendTweet(ActionEvent event) throws TwitterException {
        
        try {
                //if doesnt send exceed limit it sends tweet
                //else it display alert error message
                if(verifyCharacterLimit(tweetMessage.getText()))
                {
                    twitterEngine.createTweet(tweetMessage.getText());
                    tweetMessage.setText("");
                }
                else{
                    ErrorAlert alert = new ErrorAlert();
                    alert.displayMessage("too many characters - please don't exceed the 280 characters limit");
                }
            } catch (TwitterException ex) {
                LOG.error("Unable to send tweet", ex);
                ErrorAlert alert = new ErrorAlert();
                alert.displayMessage("Unable to send tweet");
                throw new TwitterException("Unable to send tweet", ex);
            }
    }

    /**
     * this method is called automatically after
     * the FXML file is loaded
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert tweetMessage != null : "fx:id=\"tweetText\" was not injected: check your FXML file 'TweetPageFXML.fxml'.";
        assert send != null : "fx:id=\"send\" was not injected: check your FXML file 'TweetPageFXML.fxml'.";

    }
    
    
    
    /**
     * Verifies the string doesn't exceed the 280 tweet 
     * character limit
     * 
     * @return boolean (true if it doesnt exceed, else false)
     */
    
    private boolean verifyCharacterLimit(final String tweet) 
    {
        if(tweet.length() > this.TWITTER_TWEET_LIMIT)
        {
            LOG.debug("too many characters");
            return false;
        }
        else
        {
            return true;
        }
    }

}
