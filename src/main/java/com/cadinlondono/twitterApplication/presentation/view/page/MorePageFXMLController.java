
package com.cadinlondono.twitterApplication.presentation.view.page;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This controller is part of the controller that control
 * the different pages displayed by a stack pane
 * 
 * Added show Help when help button is clicked
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class MorePageFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(MorePageFXMLController.class);
    //location of the help dialog
    private final String HELP_FXML_PATH = "/fxml/utils/helpDialog.fxml";
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="help"
    private Button help; // Value injected by FXMLLoader


    /**
     * When the user clicks on the help button
     * this method will show the help dialog page
     * @param event 
     */
    @FXML
    void showHelp(ActionEvent event) throws IOException {
        LOG.info("clicked help button");
        openHelpDialog();
    }


    /**
     * this method is called automatically after
     * the FXML file is loaded
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert help != null : "fx:id=\"help\" was not injected: check your FXML file 'MorePageFXML.fxml'.";
       
    }

    //opens up the help dialog with the user help guide
    private void openHelpDialog()  throws IOException{
         // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.HELP_FXML_PATH));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use TwitterBundle.properties
       loader.setResources(ResourceBundle.getBundle("TwitterBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (ScrollPane) loader.load();

        //set the root as the scene and return it
        Scene scene = new Scene(root);
        
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.showAndWait();     
    }
}
