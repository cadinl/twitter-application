

package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.business.MessageEngine;
import com.cadinlondono.twitterApplication.business.TwitterEngine;
import com.cadinlondono.twitterApplication.presentation.alert.ErrorAlert;
import com.cadinlondono.twitterApplication.presentation.directMessages.DirectMessages;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;
import twitter4j.User;

/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller purpose is to allow the user
 * to send a dm
 * and to retrieve the messages he has received.
 * 
 * Added verify character limit to check that the DM
 * doesn't exceed it
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class MessagePageFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(MessagePageFXMLController.class);
    
    private final int TWITTER_DM_LIMIT = 140; //the twitter dm character limit
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="users"
    private ListView<Button> users; // Value injected by FXMLLoader

    @FXML // fx:id="messages"
    private ListView<VBox> messages; // Value injected by FXMLLoader

    @FXML // fx:id="textMessage"
    private TextField textMessage; // Value injected by FXMLLoader

    @FXML // fx:id="send"
    private Button send; // Value injected by FXMLLoader
    
    @FXML // fx:id="receiverHandle"
    private TextField receiverHandle; // Value injected by FXMLLoader
    
    private final TwitterEngine twitterEngine = new TwitterEngine();//We need this to send a tweet
    private DirectMessages directMessages;
    private MessageEngine messageEngine;
    //all the users the logged in user has interacted with
    private List<User> allUsers;


    /**
     * this method is called when the send button is pressed.
     * it checks if the message in the text message field doesn't 
     * exceed the character limit.
     * It also takes the receiverHandle field to send the message to the
     * chosen receiver
     * @param event
     * @throws TwitterException 
     */
    @FXML
    void sendMessage(ActionEvent event) throws TwitterException {
            try {
                //if doesnt send exceed limit it sends tweet
                //else it display alert error message
                if(verifyCharacterLimit(textMessage.getText()))
                {
                    twitterEngine.sendDirectMessage(receiverHandle.getText(), textMessage.getText());
                    textMessage.setText("");//erase the field after sending it
                    refreshDirectMessages();
                }
                else{
                     ErrorAlert alert = new ErrorAlert();
                    alert.displayMessage("too many characters - please don't exceed the 140 characters limit");
                }
            } 
            catch (TwitterException ex) {
                LOG.error("Unable to send DM", ex);
                ErrorAlert alert = new ErrorAlert();
                alert.displayMessage("unable to send DM");
                throw new TwitterException("twitter Engine could not send DM",ex);
            }
    }

     /**
     * this method is called automatically after
     * the FXML file is loaded
     * 
     * it sets the bookmark ListView
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws TwitterException, IOException{
        assert users != null : "fx:id=\"userList\" was not injected: check your FXML file 'MessagePageFXML.fxml'.";
        assert messages != null : "fx:id=\"messages\" was not injected: check your FXML file 'MessagePageFXML.fxml'.";
        assert textMessage != null : "fx:id=\"textMessage\" was not injected: check your FXML file 'MessagePageFXML.fxml'.";
        assert send != null : "fx:id=\"send\" was not injected: check your FXML file 'MessagePageFXML.fxml'.";
        assert receiverHandle != null : "fx:id=\"receiverHandle\" was not injected: check your FXML file 'MessagePageFXML.fxml'.";

        this.directMessages = new DirectMessages();
        this.messageEngine = new MessageEngine();
        
        
    }
    /**
     * this method checks that the string
     * doesn't exceed the twitter DM character limit
     * @param tweet
     * @return 
     */
    private boolean verifyCharacterLimit(final String tweet) 
    {
        if(tweet.length() > this.TWITTER_DM_LIMIT)
        {
            LOG.debug("too many characters");
            return false;
        }
        else
        {
            return true;
        }
    }
    /**
     * sets the user buttons values after loading the fxml
     * @throws TwitterException 
     */
    public void setUserButtons()throws TwitterException{
        this.allUsers = this.messageEngine.allUsersThatUserHasInteractedWith();
        List<Button> buttons = new ArrayList<Button>();
        for(User user : allUsers)
        {
            Button userButton = new Button();
            userButton.setText(user.getScreenName());
            userButton.setOnAction(event -> setDirectMessagesListView(user));
            buttons.add(userButton);
            
        }
        //adding the refresh button
        Button refreshButton = new Button(this.resources.getString("Refresh"));
        refreshButton.setOnAction(event -> refreshDirectMessages());
        buttons.add(refreshButton);
        //revers to have the most recent conversation first
        Collections.reverse(buttons);
        this.users.setItems(FXCollections.observableArrayList(buttons));
    }
    //refreshes the direct messages(this can be long)
    private void refreshDirectMessages(){
        try
        {
        this.directMessages = new DirectMessages();
        }
        catch(TwitterException e)
        {
            LOG.error("Couldn't refresh direct message", e);
        }
    }
    

    //display the list of messages with the given user
    private void setDirectMessagesListView(User user) 
    {
        //needs to be in try and catch to assign as action to the bottom
        try
        {
            refreshDirectMessages();
            
            this.directMessages.changeToUser(user.getId());
            List<VBox> messagesList = this.directMessages.getDirectMessageList();
            Collections.reverse(messagesList);
            this.messages.setItems(FXCollections.observableArrayList(messagesList));
            this.receiverHandle.setText(user.getScreenName());
            scrollMessagesToBottom();
        }
        catch(TwitterException e)
        {
            LOG.error("Couldn't load messages", e);
        }
        catch(IOException e)
        {
            LOG.error("Couldn't open messages", e);
        }
    }
    
    private void scrollMessagesToBottom(){
        this.messages.scrollTo(this.messages.getItems().size() - 1);
    }
    
}
