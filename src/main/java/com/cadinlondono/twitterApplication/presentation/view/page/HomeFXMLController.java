
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.business.TwitterEngine;
import com.cadinlondono.twitterApplication.presentation.alert.ErrorAlert;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.fxml.FXMLLoader;
import java.io.IOException;
import twitter4j.TwitterException;

/**
 * This controller controls the main page of the application.
 * it contains the StackPane that all the pages 
 * will be displayed on
 * 
 * This is the main page.
 * it loads all twitter pages necessary
 * and controls the change pane events
 * 
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class HomeFXMLController {
    //log object
    private final static Logger LOG = LoggerFactory.getLogger(HomeFXMLController.class);
    //All the path to the fxml files to load
    private final String MESSAGE_FXML_PATH = "/fxml/page/MessagePage.fxml";
    private final String BOOKMARK_FXML_PATH = "/fxml/page/BookmarkPage.fxml";
    private final String NOTIFICATION_FXML_PATH = "/fxml/page/NotificationPage.fxml";
    private final String PROFILE_FXML_PATH = "/fxml/page/ProfilePage.fxml";
    private final String TWEET_FXML_PATH = "/fxml/page/TweetPage.fxml";
    private final String TIMELINE_FXML_PATH = "/fxml/page/TimelineList.fxml";
    private final String EXPLORE_FXML_PATH = "/fxml/page/ExplorePage.fxml";
    private final String MORE_FXML_PATH = "/fxml/page/MorePage.fxml";
    //ressource bundle is the same for all the pages
    private final String TWITTER_BUNDLE = "TwitterBundle";
    //the twitter engine object made by ken to access twitter info
    private final TwitterEngine twitterEngine = new TwitterEngine();
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="borderPane"
    private BorderPane borderPane; // Value injected by FXMLLoader

    @FXML // fx:id="home"
    private HBox home; // Value injected by FXMLLoader

    @FXML // fx:id="explore"
    private HBox explore; // Value injected by FXMLLoader

    @FXML // fx:id="notifications"
    private HBox notifications; // Value injected by FXMLLoader

    @FXML // fx:id="messages"
    private HBox messages; // Value injected by FXMLLoader

    @FXML // fx:id="bookmarks"
    private HBox bookmarks; // Value injected by FXMLLoader

    @FXML // fx:id="profile"
    private HBox profile; // Value injected by FXMLLoader

    @FXML // fx:id="more"
    private HBox more; // Value injected by FXMLLoader
    
    @FXML // fx:id="tweet"
    private HBox tweet; // Value injected by FXMLLoader

    @FXML // fx:id="stackPane"
    private StackPane stackPane; // Value injected by FXMLLoader
    //all the pages of the twitter application
    private Pane timelinePage, notificationPage, bookmarkPage, messagePage, profilePage, tweetPage, explorePage, morePage;
    //we need these controler to set variables need for their functionality
    //commonly all most all of this controller need the stackpane to be set
    private ProfilePageFXMLController profileController;
    private TimelinePageFXMLController timelineController;
    private NotificationPageFXMLController notificationController;
    private MessagePageFXMLController messageController;
    private ExplorePageFXMLController exploreController;
    private BookmarkPageFXMLController bookmarkController;
    //we use this to hide it when a new node in the stack pane needs
    //to be in front
    private Node currentNode;

    /**
     * Method called when bookmarks is clicked 
     * it changes the pane to be bookmarks page in front
     * @param event 
     */
    @FXML
    void showBookmarks(MouseEvent event) throws TwitterException, IOException {
        this.bookmarkController.setStackPane(stackPane);
        this.bookmarkController.setBookmarkedList();//display Timeline
        changePane(this.bookmarkPage);
        LOG.info("bookmark page is the front pane");
    }
    
    /**
     * Method called when explore is clicked 
     * it changes the pane to be explore page in front
     * @param event 
     */
    @FXML
    void showExplore(MouseEvent event) {
        this.exploreController.setStackPane(stackPane);
        changePane(this.explorePage);
        LOG.info("explore page is in the front pane");
    }
    /**
     * Method called when home is clicked 
     * it changes the pane to be home in front
     * @param event 
     */
    @FXML
    void showHome(MouseEvent event) throws TwitterException, IOException {
        changePane(this.timelinePage);
        prepareHome();
        LOG.info("home timeline page is in the front pane");
    }
    
    private void prepareHome() throws TwitterException, IOException {
        this.timelineController.setStackPane(this.stackPane);
        this.timelineController.refreshTimeline();
    }
    /**
     * Method called when messages is clicked 
     * it changes the pane to be messages page in front
     * @param event 
     */
    @FXML
    void showMessages(MouseEvent event) throws TwitterException, IOException {
        changePane(this.messagePage);
        this.messageController.setUserButtons();
        LOG.info("message page is in the front pane");
    }
    /**
     * Method called when more is clicked 
     * it changes the pane to be more page in front
     * @param event 
     */
    @FXML
    void showMore(MouseEvent event) {
        changePane(this.morePage);
        LOG.info("more page is in the front pane");
    }
    /**
     * Method called when notifications is clicked 
     * it changes the pane to be notifications page in front
     * @param event 
     */
    @FXML
    void showNotifications(MouseEvent event){
        changePane(this.notificationPage);
        this.notificationController.setStackpane(stackPane);
        LOG.info("notifications is in the front pane");
    }
    /**
     * Method called when profile is clicked 
     * it changes the pane to be profile page in front.
     * It also sets the user property of the page controller 
     * 
     * @param event 
     */
    @FXML
    void showProfile(MouseEvent event) throws TwitterException, IOException {
        profileController.setUser(twitterEngine.getMainUser());
        profileController.setFollowButtonInvsible();
        profileController.setStackPane(this.stackPane);
        profileController.refreshTimeline();
        changePane(this.profilePage);
        LOG.info("profile page is in the front pane");
    }
    /**
     * Method called when tweet is clicked 
     * it changes the pane to be tweet page in front
     * @param event 
     */
    @FXML
    void showTweet(MouseEvent event) {

        changePane(this.tweetPage);
        LOG.info("tweet page is in the front pane");
    }

    /**
     * this method is called automatically after
     * the FXML file is loaded
     * it also creates all the twitter pages
     * and stores them in stack pane
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException, IllegalArgumentException, TwitterException {
        assert borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert home != null : "fx:id=\"home\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert explore != null : "fx:id=\"explore\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert notifications != null : "fx:id=\"notifications\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert messages != null : "fx:id=\"messages\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert bookmarks != null : "fx:id=\"bookmarks\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert profile != null : "fx:id=\"profile\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert more != null : "fx:id=\"more\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert tweet != null : "fx:id=\"tweet\" was not injected: check your FXML file 'HomeFXML.fxml'.";
        assert stackPane != null : "fx:id=\"stackPane\" was not injected: check your FXML file 'HomeFXML.fxml'.";

        createTwitterPages();
        
    }
    
    /**
     * this method is use to load all the twitter pages
     * add them to the stack
     * and set them invisible
     * beside the timeline page which is the default page.
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws TwitterException 
     */
    private void createTwitterPages() throws IOException, IllegalArgumentException, TwitterException{
        try{
            loadTwitterPages();
            addTwitterPagesToStackPane();
            setAllStackPaneChildrenInvisible();
                 
            prepareHome();
            changePane(this.timelinePage);
        }
        catch(IOException ex){
            LOG.error("Couldn't load a twitter page", ex);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("couldn't load a twitter page");
            throw ex;
        }
        catch(IllegalArgumentException ex){
            LOG.error("get controller received a filepath that doesnt have a controller object in class", ex);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("couldn't load a twitter page");
            throw ex;
            
        }
        catch(TwitterException ex){
            LOG.error("one of the pages failed to retrieve information from twitter", ex);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("couldn't load a twitter page");
            throw ex;
        }
    }
    /**
     * This method add all the twitter pages to the stack
     * after they have been set
     */
    private void addTwitterPagesToStackPane(){
        
        this.stackPane.getChildren().add(this.profilePage);
        this.stackPane.getChildren().add(this.messagePage);
        this.stackPane.getChildren().add(this.bookmarkPage);
        this.stackPane.getChildren().add(this.notificationPage);
        this.stackPane.getChildren().add(this.tweetPage);
        this.stackPane.getChildren().add(this.timelinePage);
        this.stackPane.getChildren().add(this.morePage);
        this.stackPane.getChildren().add(this.explorePage);
    }
    /**
     * This method loads all the twitter pages 
     * with their FXML file
     * @throws IOException
     * @throws TwitterException
     * @throws IllegalArgumentException 
     */
    private void loadTwitterPages() throws IOException, TwitterException, IllegalArgumentException{
        
        this.bookmarkPage = loadPage(this.BOOKMARK_FXML_PATH);
        this.messagePage = loadPage(this.MESSAGE_FXML_PATH);
        this.notificationPage = loadPage(this.NOTIFICATION_FXML_PATH);
        this.timelinePage = loadPage(this.TIMELINE_FXML_PATH);
        this.profilePage = loadPage(this.PROFILE_FXML_PATH);
        this.tweetPage = loadPage(this.TWEET_FXML_PATH);
        this.explorePage = loadPage(this.EXPLORE_FXML_PATH);
        this.morePage = loadPage(this.MORE_FXML_PATH);
    }
    /**
     * this method loads the FXML with 
     * the file path giving as input.
     * also loads the controller if necessary
     * @param filePath
     * @return
     * @throws IOException
     * @throws TwitterException
     * @throws IllegalArgumentException 
     */
    private Pane loadPage(String filePath) throws IOException, TwitterException, IllegalArgumentException{
            
            FXMLLoader loader = new FXMLLoader();
            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(this.getClass().getResource(filePath));
            //the same resource for every page
            loader.setResources(ResourceBundle.getBundle(TWITTER_BUNDLE));
            
            Pane pane= (Pane) loader.load(); 
            
            if(shouldPageControllerBeRetrieved(filePath))
            {
               getController(loader, filePath);
            }
            
            return pane;
    }
    /**
     * gets the controller of the FXML if is necessary.
     * and stores it in is property for later use
     * @param loader
     * @param filePath
     * @throws IllegalArgumentException 
     */
    private void getController(FXMLLoader loader, String filePath) throws IllegalArgumentException, TwitterException{
        if(filePath == this.PROFILE_FXML_PATH)
        {
            this.profileController = loader.getController();
        }
        else if(filePath == this.TIMELINE_FXML_PATH)
        {
            this.timelineController = loader.getController();
        }
        else if(filePath == this.NOTIFICATION_FXML_PATH)
        {
            this.notificationController = loader.getController();
        }      
        else if(filePath == this.MESSAGE_FXML_PATH)
        {
            this.messageController = loader.getController();
        }
        else if(filePath == this.EXPLORE_FXML_PATH)
        {
            this.exploreController = loader.getController();
        }
        else if(filePath == this.BOOKMARK_FXML_PATH)
        {
            this.bookmarkController = loader.getController();
        }
        else
        {
            throw new IllegalArgumentException("the filepath did not match a controller");
        }
    }
    /**
     * this method checks if the fxml 
     * needs is controller to be retrieve
     * to access it later on
     * @param filePath
     * @return 
     */
    private boolean shouldPageControllerBeRetrieved(String filePath)
    {
        return filePath == this.PROFILE_FXML_PATH || filePath == this.TIMELINE_FXML_PATH || filePath == this.NOTIFICATION_FXML_PATH || filePath == this.MESSAGE_FXML_PATH || filePath == this.EXPLORE_FXML_PATH || filePath == this.BOOKMARK_FXML_PATH;
    }
    
    /**
     * this method is called by every onAction event handler
     * it changes the pane of the stack pane to the desired one
     * by putting it in front and making the one before invisible
     * @param toChangeTo 
     */
    private void changePane(Pane toChangeTo){
        //Panes in the stack panes are nodes
        Node nodeToChangeTo = (Node)toChangeTo;
            //setting invisible any none page pane
            Node frontPane = this.stackPane.getChildren().get(this.stackPane.getChildren().size()-1);
            frontPane.setVisible(false);
            //getting the node index to retrieve it from list
            int nodeIndex = this.stackPane.getChildren().indexOf(nodeToChangeTo);
            Node nodeToChangeToInList = this.stackPane.getChildren().get(nodeIndex);

            nodeToChangeToInList.toFront();
            nodeToChangeToInList.setVisible(true);

            currentNode = nodeToChangeToInList;
        
    }
    /**
     * this method is used mostly at initialization to 
     * set all the nodes in stack pane invisible so it
     * doesn't look like one is on top of each other
     */
    private void setAllStackPaneChildrenInvisible(){
        for(Node node : this.stackPane.getChildren())
        {
            node.setVisible(false);
        }
    }
    
    
}
