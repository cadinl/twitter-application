package com.cadinlondono.twitterApplication.presentation.view.keysform;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import com.cadinlondono.twitterApplication.business.keysbeans.KeysConfig;
import com.cadinlondono.twitterApplication.business.propertiesManager.KeysPropertiesManager;
import com.cadinlondono.twitterApplication.presentation.alert.ErrorAlert;
import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This the controller for the key properties filling form
 * it asks the user to fill in the form to populate the keys properties file
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class MissingPropertiesFormFXMLController {

    private final static Logger LOG = LoggerFactory.getLogger(MissingPropertiesFormFXMLController.class);
    
    private final String HOME_FILE_PATH = "/fxml/page/Home.fxml";
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="consumerKey"
    private TextField consumerKey; // Value injected by FXMLLoader

    @FXML // fx:id="consumerSecret"
    private TextField consumerSecret; // Value injected by FXMLLoader

    @FXML // fx:id="accessToken"
    private TextField accessToken; // Value injected by FXMLLoader

    @FXML // fx:id="AccessTokenSecret"
    private TextField AccessTokenSecret; // Value injected by FXMLLoader

    @FXML // fx:id="Exit"
    private Button Exit; // Value injected by FXMLLoader

    @FXML // fx:id="Create"
    private Button Create; // Value injected by FXMLLoader

    KeysConfig keyBeans;
    Scene scene;
    Stage stage;
    
    /**
     * constructor creates a key bean
     */
    public MissingPropertiesFormFXMLController(){
        super();
        keyBeans = new KeysConfig();
    }
    /**
     * this method is use to set the stage properties of the class.
     * this is needed to switch to the home scene once 
     * the form has been completed
     * @param stage 
     */
    public void setSceneStageController(Stage stage) {
        this.stage = stage;
    }
    
    /**
     * create button event handler.
     * creates or populates the twitter4j properties file
     * with the keys written in the form.
     * @param event 
     */
    @FXML
    void CreatePropertiesKeys(ActionEvent event) {
        KeysPropertiesManager keysManager = new KeysPropertiesManager();
        try {
            keysManager.writeKeysProperties("", "twitter4j", keyBeans);

            // Change the scene on the stage
            // Instantiate the FXMLLoader
            FXMLLoader loader = new FXMLLoader();

            // Set the location of the fxml file in the FXMLLoader
            loader.setLocation(this.getClass().getResource(this.HOME_FILE_PATH));

            // Localize the loader with its bundle
            loader.setResources(ResourceBundle.getBundle("TwitterBundle"));

           
            // Parent is the base class for all nodes that have children in the
            // scene graph such as AnchorPane and most other containers
            Parent root = (BorderPane) loader.load();
            Scene homeScene = new Scene(root);
            this.stage.setScene(homeScene);
            
        } catch (IOException ex) {
            LOG.error("onSave error", ex);
            ErrorAlert alert = new ErrorAlert();
            alert.displayMessage("onSave() on error");
            Platform.exit();
        }
    }

    /**
     * This is the event handler of the exit button
     * it exits the application
     * @param event 
     */
    @FXML
    void ExitProgram(ActionEvent event) {
        Platform.exit();
    }

    /**
     * binds the text fields to the java bean property setter method
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        Bindings.bindBidirectional(consumerKey.textProperty(), keyBeans.consumerKeyProperty());
        Bindings.bindBidirectional(consumerSecret.textProperty(), keyBeans.consumerSecretProperty());
        Bindings.bindBidirectional(accessToken.textProperty(), keyBeans.accessTokenProperty());
        Bindings.bindBidirectional(AccessTokenSecret.textProperty(), keyBeans.accessTokenSecretProperty());
    }
    
}
