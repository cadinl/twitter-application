
package com.cadinlondono.twitterApplication.presentation.view.page;

import com.cadinlondono.twitterApplication.presentation.timeline.TimelineTweets;
import com.cadinlondono.twitterApplication.presentation.view.message.NotificationFXMLController;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import twitter4j.TwitterException;

/**
 * This controller is part of the controllers that control
 * the different pages displayed by a stack pane
 * 
 * This controller uses a list view to display notifications and mentions
 * of the user
 * 
 * Added mention and notification buttons
 * to change the list view content depending
 * on what the user wants to see
 * 
 * 
 * @author Cadin Londono
 * @version 1.0
 */
public class NotificationPageFXMLController {
    
    //LOG object
    private final static Logger LOG = LoggerFactory.getLogger(NotificationPageFXMLController.class);

    private final String NOTIFICATION_FILE_PATH = "/fxml/message/Notification.fxml";
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="borderPane"
    private BorderPane borderPane; // Value injected by FXMLLoader

    @FXML // fx:id="retweetsByYou"
    private Button retweetsByYou; // Value injected by FXMLLoader

    @FXML // fx:id="yourTweetsRetweeted"
    private Button yourTweetsRetweeted; // Value injected by FXMLLoader
    
    @FXML // fx:id="mentions"
    private Button mentions; // Value injected by FXMLLoader

    @FXML // fx:id="notifcations"
    private ListView<VBox> notifcations; // Value injected by FXMLLoader
    
    private TimelineTweets timelineTweets;
    private List<VBox> tweetList;
    private StackPane stackPane;
    

    /**
     * event handler for the retweetsByYou button that shows 
     * the retweets by the logged in user
     * @param event
     * @throws TwitterException
     * @throws IOException 
     */
    @FXML
    void showRetweetsByYou(ActionEvent event) throws TwitterException, IOException {
        try{
            this.refreshYourRetweets();
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }

    /**
     * event handler for the yourTweetRetweeted button
     * @param event
     * @throws TwitterException
     * @throws IOException 
     */
    @FXML
    void showYourTweetsRetweeted(ActionEvent event) throws TwitterException, IOException {
        try{
            this.refreshYourTweetsRetweeted();
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }


    /**
     * When the user clicks on the mentions button
     * it changes the content of the ListView
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    void showMentions(ActionEvent event) throws IOException, TwitterException {
        try{
            refreshMentions();
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    
    }

    /**
     * this method is called automatically after
     * the FXML file is loaded
     */
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() throws IOException {
        assert borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'NotificationPageFXML.fxml'.";
        assert mentions != null : "fx:id=\"mentions\" was not injected: check your FXML file 'NotificationPageFXML.fxml'.";
        assert notifcations != null : "fx:id=\"notifcations\" was not injected: check your FXML file 'NotificationPageFXML.fxml'.";

    }
    
    /**
     * sets the stackpane to be able to give the tweets the functionality to 
     * display user profile
     * @param stackPane 
     */
    public void setStackpane(StackPane stackPane){
        this.stackPane = stackPane;
        
    }
   
    

    /**
     * refreshes the mentions by restarting all of the objects
     * necessary to display a timeline (for mentions)
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshMentions()throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList();
        createMentions();
    }
    
    /**
     * refreshes the mentions by restarting all of the objects
     * necessary to display a timeline for(your tweets retweeted)
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshYourTweetsRetweeted() throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList();
        createYourTweetsRetweeted();
    }
    
    /**
     * refreshes the mentions by restarting all of the objects
     * necessary to display a timeline (for logged in retweets)
     * @throws TwitterException
     * @throws IOException 
     */
    public void refreshYourRetweets() throws TwitterException, IOException{
        this.timelineTweets = new TimelineTweets();
        this.tweetList = new ArrayList();
        createYourRetweets();
    }
    
    /**
     * retrieves the user tweets retweeted list and displays it 
     * in the list view
     * @throws TwitterException
     * @throws IOException 
     */
    public void createYourRetweets() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeToUserRetweets();
            if(!timelineTweets.getTimelineList().isEmpty())
            {
                this.tweetList.addAll(timelineTweets.getTimelineList());
            }
            this.notifcations.setItems(FXCollections.observableArrayList(tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    
    /**
     * retrieves the user tweets retweeted list and displays it 
     * in the list view
     * @throws TwitterException
     * @throws IOException 
     */
    public void createYourTweetsRetweeted() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeUserTweetsRetweeted();
            if(!timelineTweets.getTimelineList().isEmpty())
            {
                this.tweetList.addAll(timelineTweets.getTimelineList());
            }
            this.notifcations.setItems(FXCollections.observableArrayList(tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    
    /**
     * this method set the timeline list view property
     * by getting all the VBox items from the timeline tweets
     * class
     * @throws TwitterException 
     */
    public void createMentions() throws TwitterException, IOException{
        try{
            timelineTweets.setStackPane(stackPane);
            this.timelineTweets.changeToMentions();
            if(!timelineTweets.getTimelineList().isEmpty())
            {
            this.tweetList.addAll(timelineTweets.getTimelineList());
            addMoreButton(tweetList);
            }
            this.notifcations.setItems(FXCollections.observableArrayList(tweetList));
        }
        catch(TwitterException ex){
            LOG.error("couldn't load timeline listview, might need to recheck twitter keys properties", ex);
            throw ex;
        }
    }
    //adds the button to see more tweets in the next page
    private void addMoreButton(List<VBox> tweetList){
        VBox buttonPane = new VBox();
        Button moreButton = new Button(this.resources.getString("More"));
        moreButton.setOnAction(event -> moreButtonEvent());
        buttonPane.getChildren().add(moreButton);
        tweetList.add(buttonPane);
    }
    //event handler for the more object(try and catch because it doesn't let me throw
    //an exception)
    private void moreButtonEvent(){
        try
        {
            this.timelineTweets.nextPage();
            createMentions();
        }
        catch(IOException ex)
        {
            LOG.error("couldn't load fxml of timeline next page", ex);
        }
        catch(TwitterException ex)
        {
            LOG.error("couldn't load timeline next page from twitter", ex);
        }
        
    }
}
