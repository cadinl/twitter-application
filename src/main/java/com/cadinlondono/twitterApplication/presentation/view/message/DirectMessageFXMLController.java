
package com.cadinlondono.twitterApplication.presentation.view.message;

import com.cadinlondono.twitterApplication.business.UserEngine;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import twitter4j.User;
import twitter4j.DirectMessage;
import twitter4j.TwitterException;

public class DirectMessageFXMLController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="profilePicture"
    private ImageView profilePicture; // Value injected by FXMLLoader

    @FXML // fx:id="screeName"
    private Label screeName; // Value injected by FXMLLoader

    @FXML // fx:id="textMessage"
    private Label textMessage; // Value injected by FXMLLoader
    
    private DirectMessage directMessage;
    private User user;
    private StackPane stackPane;

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert profilePicture != null : "fx:id=\"profilePicture\" was not injected: check your FXML file 'DirectMessage.fxml'.";
        assert screeName != null : "fx:id=\"screeName\" was not injected: check your FXML file 'DirectMessage.fxml'.";
        assert textMessage != null : "fx:id=\"textMessage\" was not injected: check your FXML file 'DirectMessage.fxml'.";

    }
    /**
     * sets the direct message that this VBox will contain
     * @param directMessage
     * @throws TwitterException 
     */
    public void setDirectMessage(DirectMessage directMessage) throws TwitterException
    {
        this.directMessage = directMessage;
        UserEngine userEngine = new UserEngine(this.directMessage.getSenderId());
        setUser(userEngine.getUser());
        this.textMessage.setText(this.directMessage.getText());
    }
    /**
     * sets the user that this dm is from
     * @param user 
     */
    public void setUser(User user)
    {
        this.user = user;
        setUserContent();
    }
    /**
     * sets the StackPane to be able to switch panes
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    
    private void setUserContent(){
        this.profilePicture.setImage(new Image(this.user.getOriginalProfileImageURL()));
        this.screeName.setText(this.user.getScreenName());
    }
    
}
