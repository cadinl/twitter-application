
package com.cadinlondono.twitterApplication.presentation.alert;

import com.cadinlondono.twitterApplication.business.TweetEngine;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class CommentDialogController {
    
    //LOG
    private final static Logger LOG = LoggerFactory.getLogger(CommentDialogController.class);

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="replyText"
    private TextArea replyText; // Value injected by FXMLLoader
    
    @FXML // fx:id="reply"
    private Button reply; // Value injected by FXMLLoader
    
    private Tweet tweet;//tweet that the user is replying to
    private TweetEngine tweetEngine;

    /**
     * event handler for the reply button.
     * Takes the text in the text area
     * and uses to reply to the tweet set
     * @param event
     * @throws TwitterException 
     */
    @FXML
    void replyToTweet(ActionEvent event) throws TwitterException {
        String replyMessage = replyText.getText();
        this.tweetEngine.replyToTweet(replyMessage);
        LOG.info("you have succesfully replied to the tweet");
        
        //closing the window after sending reply
        Stage stage = (Stage) reply.getScene().getWindow();//any of the javafx components have reference to the scene
        stage.close();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert reply != null : "fx:id=\"reply\" was not injected: check your FXML file 'commentDialog.fxml'.";
        assert replyText != null : "fx:id=\"replyText\" was not injected: check your FXML file 'commentDialog.fxml'.";

    }
    
    /**
     * sets the tweet that the user is commenting on
     * @param tweet
     * @throws TwitterException 
     */
    public void setTweet(Tweet tweet) throws TwitterException{
        this.tweet = tweet;
        this.tweetEngine = new TweetEngine(tweet);
    }
}
