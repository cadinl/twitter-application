

package com.cadinlondono.twitterApplication.presentation.alert;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 *This object is used to create an alert
 * that displays a certain message
 * @author cadin
 */
public class ErrorAlert {
    /**
     * displays an error message with the message
     * as is content
     * @param message 
     */
    public void displayMessage(String message){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setContentText(message);
        alert.show();
    }
}
