

package com.cadinlondono.twitterApplication.presentation.alert;

import com.cadinlondono.twitterApplication.business.TweetEngine;
import com.cadinlondono.twitterApplication.business.tweetBeans.Tweet;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.TwitterException;

public class RetweetDialogController {

    //LOG
    private final static Logger LOG = LoggerFactory.getLogger(RetweetDialogController.class);

    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="replyText"
    private TextArea replyText; // Value injected by FXMLLoader

    @FXML // fx:id="retweet"
    private Button retweet; // Value injected by FXMLLoader

    @FXML // fx:id="retweetComment"
    private Button retweetComment; // Value injected by FXMLLoader

    private Tweet tweet;
    private TweetEngine tweetEngine;
    
    /**
     * event handler for the retweet button.
     * retweets the tweet ignoring the text in the 
     * text area
     * @param event
     * @throws TwitterException 
     */
    @FXML
    void retweetTweet(ActionEvent event) throws TwitterException {
        tweetEngine.retweetTweet();
        LOG.info("retweeted tweet");
        closeWindow();
    }

    /**
     * event handle for the retweetWithComment button.
     * it retweets and adds the comment inputed in the 
     * text area
     * @param event
     * @throws TwitterException 
     */
    @FXML
    void retweetTweetWithComment(ActionEvent event) throws TwitterException{
        String message = this.replyText.getText();
        tweetEngine.retweetTweetWithMessage(message);
        LOG.info("retweeted tweet with message: " +message);
        closeWindow();
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert replyText != null : "fx:id=\"replyText\" was not injected: check your FXML file 'retweetDialog.fxml'.";
        assert retweet != null : "fx:id=\"retweet\" was not injected: check your FXML file 'retweetDialog.fxml'.";
        assert retweetComment != null : "fx:id=\"retweetComment\" was not injected: check your FXML file 'retweetDialog.fxml'.";

    }
    
    /**
     * sets the tweet that the user wishes to retweet
     * @param tweet
     * @throws TwitterException 
     */
    public void setTweet(Tweet tweet) throws TwitterException{
        this.tweet = tweet;
        this.tweetEngine = new TweetEngine(tweet);
    }
    //closes the dialog. Used after retweeting
    private void closeWindow(){
        Stage stage = (Stage)this.retweet.getScene().getWindow();
        stage.close();
    }
}
