
package com.cadinlondono.twitterApplication.presentation.directMessages;

import com.cadinlondono.twitterApplication.business.TwitterEngine;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import twitter4j.User;
import twitter4j.TwitterException;

/**
 * This class is used to send a dm via clicking send dm
 * in the profile of a user
 * @author cadin
 */
public class DirectMessageUtilityFXMLController {

    //LOG object
    private final static Logger LOG = LoggerFactory.getLogger(DirectMessageUtilityFXMLController.class);
    
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="text"
    private TextArea text; // Value injected by FXMLLoader

    @FXML // fx:id="send"
    private Button send; // Value injected by FXMLLoader
    
    @FXML // fx:id="goBackToProfile"
    private Button goBackToProfile; // Value injected by FXMLLoader
    
    //the user we want to send a direct message to
    private User user;
    //the stackpane to change pane
    private StackPane stackPane;
    //the engine to send a dm directly
    private TwitterEngine twitterEngine;
    
    /**
     * hides this pane to show previous pane
     * which is the user profile pane
     * @param event 
     */
    @FXML
    void goBackToUserProfile(ActionEvent event) {
        hideThisPane();
    }

    /**
     * sends a message with the twitter engine 
     * to the user 
     * @param event
     * @throws TwitterException 
     */
    @FXML // event handler for the send button
    void sendMessage(ActionEvent event) throws TwitterException{
        twitterEngine = new TwitterEngine();
        twitterEngine.sendDirectMessage(user.getScreenName(), this.text.getText());
        hideThisPane();
        LOG.info("sent message");
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert text != null : "fx:id=\"text\" was not injected: check your FXML file 'directMessageUtility.fxml'.";
        assert send != null : "fx:id=\"send\" was not injected: check your FXML file 'directMessageUtility.fxml'.";
        assert goBackToProfile != null : "fx:id=\"goBackToProfile\" was not injected: check your FXML file 'directMessageUtility.fxml'.";


    }
    /**
     * set the stackpane to be able to change panes after sending message
     * @param stackPane 
     */
    public void setStackPane(StackPane stackPane)
    {
        this.stackPane = stackPane;
    }
    /**
     * setting the user that we want to send a direct message to
     * @param user 
     */
    public void setUser(User user)
    {
        this.user = user;
    }
    
    private void hideThisPane(){
        Node backgroundPane = this.stackPane.getChildren().get(this.stackPane.getChildren().size()-2);
        backgroundPane.setVisible(true);
        this.stackPane.getChildren().remove(this.stackPane.getChildren().size() -1);
    }
}
