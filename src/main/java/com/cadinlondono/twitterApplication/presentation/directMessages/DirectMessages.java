
package com.cadinlondono.twitterApplication.presentation.directMessages;

import com.cadinlondono.twitterApplication.business.MessageEngine;
import com.cadinlondono.twitterApplication.presentation.view.message.DirectMessageFXMLController;
import java.io.IOException;
import java.util.ArrayList;
import twitter4j.TwitterException;
import twitter4j.DirectMessage;
import java.util.List;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *This class is use to transform a direct message
 * object from twitter4j to a direct message in fxml
 * @author cadin
 */
public class DirectMessages {
    
    private final static Logger LOG = LoggerFactory.getLogger(DirectMessages.class);
    
    private final String DM_FXML_PATH = "/fxml/message/DirectMessage.fxml";
    
    private MessageEngine messageEngine;
    private List<DirectMessage> allMessages;
    /**
     * constructor get all the direct messages from 
     * the message engine
     * @throws TwitterException 
     */
    public DirectMessages() throws TwitterException{
        messageEngine = new MessageEngine();
        allMessages = messageEngine.getDirectMessages();
    }
   /**
    * filters the direct messages to only be related to
    * a specific user
    * @param userId
    * @throws TwitterException 
    */
    public void changeToUser(long userId) throws TwitterException{
        this.allMessages = this.messageEngine.getDirectMessagesByUser(userId);
    }
    /**
     * returns a list of vbox containing all the direct messages
     * that were loaded in the direct message list class variable
     * @return List of VBox
     * @throws TwitterException
     * @throws IOException 
     */
    public List<VBox> getDirectMessageList() throws TwitterException, IOException{
        List<VBox> directMessages = new ArrayList<>();
        
        for(DirectMessage directMessage : this.allMessages)
        {
            VBox directMessagePane = createDirectMessagePane(directMessage); 
            directMessages.add(directMessagePane);
        }
        
        return directMessages;
    }

    private VBox createDirectMessagePane(DirectMessage directMessage) throws IOException, TwitterException{
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.DM_FXML_PATH));


        //loading the VBox
        VBox tweetContainer = (VBox) loader.load();
        //getting the controller
        DirectMessageFXMLController controller = loader.getController();
       
        controller.setDirectMessage(directMessage);
        
        return tweetContainer; 
    }
}
