package com.cadinlondono.twitterApplication.presentation;

import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.cadinlondono.twitterApplication.presentation.view.keysform.MissingPropertiesFormFXMLController;
import com.cadinlondono.twitterApplication.business.keysbeans.KeysConfig;
import com.cadinlondono.twitterApplication.business.propertiesManager.KeysPropertiesManager;
import java.io.IOException;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

/**
 * Based on code from https://www.baeldung.com/twitter4j
 *
 * this the class that launches the application
 * and sets the main scene
 * @author Ken Fogel
 */
public class TwitterApplication extends Application {

    //LOG Object
    private final static Logger LOG = LoggerFactory.getLogger(TwitterApplication.class);
    //paths to the fxml needed to be loaded
    private final String HOME_FILE_PATH = "/fxml/page/Home.fxml";
    private final String KEYFORM_FILE_PATH = "/fxml/missingPropertiesForm.fxml";
    private final String KEYFORM_FILE_STYLE_PATH = "/styles/missingpropertiesformfxml.css";
    
    private Scene homeScene;//twitter home page
    private Stage stage;
    
    /**
     * Where it begins
     * @param args 
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    
    /**
     * JavaFX begins at start
     * 
     * @param primaryStage
     * @throws Exception 
     */
    @Override
    public void start(Stage primaryStage){
        //set stage
        this.stage = primaryStage;
        //if it doesnt have a twitter4j properties file or the values in it are null
        //than load properties form else load home scene
        if (!checkForProperties()) {
            setKeyPropertiesSceneAsMainScene();
        } 
        else {
            setHomeSceneAsMainScene();   
        }

        this.stage.setTitle("Twitter Application");

        this.stage.show();
        
        //Java fx might not kill the processes if we exit with the x button 
        //of the window, so we have to close the program ourselves
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
          public void handle(WindowEvent we) {
              LOG.info("closed app");
              Platform.exit();
          }
        }); 
    }
    /**
     * loads the home scene and sets it 
     * as the scene to display
     */
    private void setHomeSceneAsMainScene(){
        try
            {
                this.homeScene = createHomeScene();
                this.stage.setScene(this.homeScene);
            }
            catch(Exception ex)
            {
                LOG.error("application closed with this exception", ex);
               Platform.exit();
            }
    }
    /**
     * loads the properties form scene and sets it 
     * as the scene to display
     */
    private void setKeyPropertiesSceneAsMainScene(){
        try{
                Scene propertiesForm = createPropertiesForm();
                propertiesForm.getStylesheets().add(this.KEYFORM_FILE_STYLE_PATH);
                this.stage.setScene(propertiesForm);
            }
            catch(IOException ex){
                LOG.error("application closed when opening keyform", ex);
                Platform.exit();
            }
    }
    
    /**
     * creates the home scene with the fxml loader
     * @return
     * @throws IOException 
     */
    private Scene createHomeScene() throws IOException{
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.HOME_FILE_PATH));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
       loader.setResources(ResourceBundle.getBundle("TwitterBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (BorderPane) loader.load();

        //set the root as the scene and return it
        Scene scene = new Scene(root);
        return scene;
    }

    /**
     * creates the properties form with fxml loader
     * @return
     * @throws IOException 
     */
    private Scene createPropertiesForm() throws IOException {
        // Instantiate the FXMLLoader
        FXMLLoader loader = new FXMLLoader();

        // Set the location of the fxml file in the FXMLLoader
        loader.setLocation(this.getClass().getResource(this.KEYFORM_FILE_PATH));

        // Localize the loader with its bundle
        // Uses the default locale and if a matching bundle is not found
        // will then use MessagesBundle.properties
        loader.setResources(ResourceBundle.getBundle("KeyBundle"));

        // Parent is the base class for all nodes that have children in the
        // scene graph such as AnchorPane and most other containers
        Parent root = (VBox) loader.load();

        // Retrieve a reference to the controller so that we can pass
        // in the properties object
        MissingPropertiesFormFXMLController controller = loader.getController();

        // Pass the references that the properties controller will need to
        // display the second scene after entering the mail config data
        controller.setSceneStageController( stage);

        Scene scene = new Scene(root);
        return scene;
    }
    /**
     * this method checks if the properties file for twitter exist
     * and if the values are not null
     * if all conditions met it returns true
     * @return true if is found else returns false
     */
    private boolean checkForProperties() {
        boolean found = false;
        KeysConfig keysConfig = new KeysConfig();//Java bean
        KeysPropertiesManager kpm = new KeysPropertiesManager();//use to manipulate bean

        try {
            if (kpm.loadKeysProperties(keysConfig, "", "twitter4j")) {
                if(checkForNullProperties(keysConfig))
                {
                    found = true;
                }
            }
        } catch (IOException ex) {
            LOG.error("checkForProperties error", ex);
        }
        return found;
    }
    /**
     * method checks if the properties in the bean are not null
     * @param keysConfig
     * @return 
     */
    private boolean checkForNullProperties(KeysConfig keysConfig) {
        if(keysConfig.getConsumerKey().equals("")|| keysConfig.getConsumerSecret().equals("") || keysConfig.getAccessToken().equals("") || keysConfig.getAccessTokenSecret().equals(""))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
            

}
